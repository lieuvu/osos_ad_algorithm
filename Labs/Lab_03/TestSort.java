
import java.net.URLDecoder;
import java.lang.StringBuilder;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class TestSort {
	// some class constatns
	private static final File classFile = new File(TestSort.class.getResource("TestSort.class").getPath());
	

	// constant for testing
	private static final int N = 10;
	private static final int STARTN = 100;
	private static final int INC = 100;
	private static final String THOUSAND_OPT = "THOUSAND";

	/**
	 * Checks if a generic array is sorted
	 * 
	 * @param arrayP - Generic array to be checked
	 * @return - true if an array is sorted or false if an array is unsorted
	 */
	private static <T extends Comparable<T>> boolean checkArrSorted(T[] arrP) {
		boolean isSorted = true;
		
		for (int i = 1; i < arrP.length; i++) {
			if (arrP[i].compareTo(arrP[i-1]) < 0) {
				isSorted = false;
				break;
			}
		}
	
		return isSorted;
	}

	/**
	 * Writes to file with certain file name
	 * 
	 * @param fileN - File to be written
	 * @param sb - String Builder of all the data
	 * @throws IOException
	 */
	private static void writeToFile(final String fileN, final StringBuilder sb) throws IOException {
		String outFileName;
		if (fileN == null)
			outFileName = URLDecoder.decode(classFile.getParent(), "UTF-8") + File.separator + "data.csv";
		else
			outFileName = URLDecoder.decode(classFile.getParent(), "UTF-8") + File.separator + fileN;

		PrintWriter writer = new PrintWriter(outFileName, "UTF-8");
		writer.print(sb.toString());
		writer.close();
		System.out.println("Done Writing To File!");
	}

	/**
	 * Writes to file with default file name
	 * 
	 * @param sb - String Builder of all the data
	 * @throws IOException
	 */
	private static void writeToFile(final StringBuilder sb) throws IOException {
		writeToFile(null, sb);
	}

	/**
	 * Tests different sorting algorithm
	 */
	private static void test() {
		int arrayN = STARTN;
		StringBuilder result = new StringBuilder();
		Stopwatch sw = new Stopwatch();

		try {
			sw.configExperiments(50, 22);
			result.append("N;BubleSort;InsertionSort;QuickSort;Built-inSort\n");
			for (int i = 0; i < N; i++) {
				BubbleSort bubSort = new BubbleSort(arrayN);
				InsertionSort insSort = new InsertionSort(arrayN);
				QuickSort qSort = new QuickSort(arrayN);
				BuiltInArrSort buInSort = new BuiltInArrSort(arrayN);

				result.append(arrayN);
				// bubble sort
				sw.measure(bubSort); result.append(";"); sw.toValue(result, THOUSAND_OPT);
				// insertion sort
				sw.measure(insSort); result.append(";"); sw.toValue(result, THOUSAND_OPT);
				// quick sort
				sw.measure(qSort); result.append(";"); sw.toValue(result, THOUSAND_OPT);
				// built-in sort
				sw.measure(buInSort); result.append(";"); sw.toValue(result, THOUSAND_OPT);

				if (i < N-1)
					result.append("\n");

				System.out.println("N: " + arrayN);
				//bubSort.print();
				System.out.println("is Sorted B- " + checkArrSorted(bubSort.getArray()));
				//insSort.print();
				System.out.println("is Sorted I- " + checkArrSorted(insSort.getArray()));
				System.out.println("is Sorted Q- " + checkArrSorted(qSort.getArray()));
				System.out.println("is Sorted BuIn- " + checkArrSorted(qSort.getArray()));
				System.out.println("----------");

				arrayN += INC;				
			}
		
			writeToFile(result);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		test();
	}

}