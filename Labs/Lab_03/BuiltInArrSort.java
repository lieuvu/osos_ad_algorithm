
import java.util.Arrays;

public class BuiltInArrSort<T extends Comparable<T>> implements Stopwatch.Test {
	private int size;
	private T[] inArr;

	/**
	 * Constructor of BuiltInArrSort
	 *
	 * @param size - Size of the generic array to be sorted
	 */
	public BuiltInArrSort(final int size) {
		this.size = size;
	}

	/**
	 * Constructor of BuiltInArrSort
	 *
	 * @param arrayP - Generic array to be sorted
	 */
	public BuiltInArrSort(final T[] arrayP) {
		this.inArr = (T[]) Arrays.copyOf(arrayP, arrayP.length);
	}

	/**
	 * Sorts an array using built-in Arrays.sort()
	 */
	public void sort() {
		Arrays.sort(inArr);
	}

	/**
	 * Prints a generic array
	 */
	public void print() {
		System.out.println("----- Bubble Sort -----");
		for (int i = 0; i < inArr.length; i++) {
			System.out.format("%1$8d : %2$s\n", i, String.valueOf(inArr[i]));
		}
		System.out.println("--------------------------");
	}

	/**
	 * Gets a generic array
	 */
	public T[] getArray() {
		return inArr;
	}

	/**
	 * Test sort function
	 */
	@Override
	public void test() {
		sort();
	}

	/**
	 * Setup a test
	 */
	@Override
	public void setup() {
		this.inArr = RandomArray.randomStrArr(size);
	}
}