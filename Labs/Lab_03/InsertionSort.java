
import java.util.Arrays;

public class InsertionSort<T extends Comparable<T>> implements Stopwatch.Test {
	private int size;
	private T[] inArr;

	/**
	 * Constructor of InsertionSort
	 *
	 * @param size - Size of the generic array to be sorted
	 */
	public InsertionSort (final int size) {
		this.size = size;
	}

	/**
	 * Constructor of InsertionSort
	 *
	 * @param arrayP - Generic array to be sorted
	 */
	public InsertionSort(final T[] arrayP) {
		this.inArr = (T[]) Arrays.copyOf(arrayP, arrayP.length);
	}

	/**
	 * Sorts an array using insertion sort
	 */
	public void sort() {
		for (int i = 1; i < inArr.length; i++) {
			T curr = inArr[i];
			int j;

			for (j = i - 1; j >= 0 && curr.compareTo(inArr[j]) < 0; j--) {
				inArr[j+1] = inArr[j];
			}
			
			inArr[j+1] = curr;
		}
	}

	/**
	 * Prints a generic array
	 */
	public void print() {
		System.out.println("----- Insertion Sort -----");
		for (int i = 0; i < inArr.length; i++) {
			System.out.format("%1$8d : %2$s\n", i, String.valueOf(inArr[i]));
		}
		System.out.println("--------------------------");
	}

	/**
	 * Gets a generic array
	 */
	public T[] getArray() {
		return inArr;
	}
	
	/**
	 * Test sort function
	 */
	@Override
	public void test() {
		sort();
	}
	
	/**
	 * Setup a test
	 */
	@Override
	public void setup() {
		this.inArr = RandomArray.randomStrArr(size);
	}
}