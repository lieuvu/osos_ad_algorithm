
import java.util.Arrays;

public class QuickSort<T extends Comparable<T>> implements Stopwatch.Test {
	private int size;
	private T[] inArr;

	/**
	 * Constructor of QuickSort
	 *
	 * @param size - Size of the generic array to be sorted
	 */
	public QuickSort(final int size) {
		this.size = size;
	}

	/**
	 * Constructor of QuickSort
	 *
	 * @param arrayP - Generic array to be sorted
	 */
	public QuickSort(final T[] arrayP) {
		this.inArr = (T[]) Arrays.copyOf(arrayP, arrayP.length);
	}

	/**
	 * Sorts a generic array by calling quickSort function
	 *
	 * @param arrayP - Generic array to be sorted
	 * @param left - Left index of the array
	 * @param right - Right index of the array
	 */
	public void sort(final T[] arrayP, final int left, final int right) {
		quickSort(arrayP, left, right);
	}

	/**
	 * Sorts a generic array using quick sort
	 *
	 * @param arrayP - Generic array to be sorted
	 * @param left - Left index of the array
	 * @param right - Right index of the array
	 */
	private void quickSort(final T[] arrayP, final int left, final int right) {
		if (left < right) {
			int pIdx = partition(arrayP, left, right);
			quickSort(arrayP, left, pIdx-1);
			quickSort(arrayP, pIdx+1, right);
		}
	}

	/**
	 * Partiioning an array
	 *
	 * @param arrayP - Generic array to be sorted
	 * @param left - Left index of a partition
	 * @param right - Right index of a partion
	 * @return - Index position of a middle partition
	 */
	private int partition(T[] arrayP, final int left, final int right) {
		T pivot = null;
		int pIdx;

		// select the pivot and swap it
		// with the last element of the array
		pivot =  arrayP[left];		
		arrayP[left] = arrayP[right];
		arrayP[right] = pivot;

		// set the pivot index
		pIdx = left;

		// loop through an array and put smaller elements
		// to the left and larger elements to the right
		// of the pivot
		for (int i = left; i <= right-1; i++) {
			//System.out.println("Element-" + arrayP[i] + "\t" + "Pivot-" + pivot);
			if (arrayP[i].compareTo(pivot) <= 0) {
				// swap the element with the element at pivot index
				T temp = arrayP[pIdx];
				arrayP[pIdx] = arrayP[i];
				arrayP[i] = temp;
				pIdx++;
			}
		}

		// put the pivot back to the pivot index
		arrayP[right] = arrayP[pIdx];
		arrayP[pIdx] = pivot;

		return pIdx;
	}

	/**
	 * Prints a generic array
	 */
	public void print() {
		System.out.println("----- Quick Sort -----");
		for (int i = 0; i < inArr.length; i++) {
			System.out.format("%1$8d : %2$s\n", i, String.valueOf(inArr[i]));
		}
		System.out.println("--------------------------");
	}

	/**
	 * Gets a generic array
	 */
	public T[] getArray() {
		return inArr;
	}

	/**
	 * Test sort function
	 */
	@Override
	public void test() {
		sort(inArr, 0, inArr.length-1);
	}
	
	/**
	 * Setup a test
	 */
	@Override
	public void setup() {
		//this.inArr = RandomArray.randomStrArr(size);
		this.inArr = RandomArray.randomIntArr(size);
	}
}