
import java.util.Arrays;

public class BubbleSort<T extends Comparable<T>> implements Stopwatch.Test {
	private int size;
	private T[] inArr;

	/**
	 * Constructor of BubbleSort
	 *
	 * @param size - Size of the generic array to be sorted
	 */
	public BubbleSort(final int size) {
		this.size = size;
	}

	/**
	 * Constructor of BubbleSort
	 *
	 * @param arrayP - Generic array to be sorted
	 */
	public BubbleSort(final T[] arrayP) {
		this.inArr = (T[]) Arrays.copyOf(arrayP, arrayP.length);
	}

	/**
	 * Sorts an array using bubble sort
	 */
	public void sort() {
		boolean swapped;
		do {
			swapped = false;
			for (int i = 1; i < inArr.length; i++) {
				if (inArr[i-1].compareTo(inArr[i]) > 0) {
					T temp;
					temp = inArr[i-1];
					inArr[i-1] = inArr[i];
					inArr[i] = temp;
					swapped = true;
				}
			}
		} while (swapped);
	}

	/**
	 * Prints a generic array
	 */
	public void print() {
		System.out.println("----- Bubble Sort -----");
		for (int i = 0; i < inArr.length; i++) {
			System.out.format("%1$8d : %2$s\n", i, String.valueOf(inArr[i]));
		}
		System.out.println("--------------------------");
	}

	/**
	 * Gets a generic array
	 */
	public T[] getArray() {
		return inArr;
	}

	/**
	 * Test sort function
	 */
	@Override
	public void test() {
		sort();
	}

	/**
	 * Setup a test
	 */
	@Override
	public void setup() {
		this.inArr = RandomArray.randomStrArr(size);
	}
}