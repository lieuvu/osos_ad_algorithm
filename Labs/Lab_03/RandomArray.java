
import java.lang.StringBuilder;
import java.security.SecureRandom;

public class RandomArray {
	// some class constants
	private static final int STR_LENGTH = 32;
	private static final int RAND_RANGE_MAX = 3;
	private static final int RAND_RANGE_MIN = 1;
	private static final int CHAR_LENGTH = 26;
	private static final int NUM_LENGTH = 10;
	private static final int NUM_CODE = 48;
	private static final int CHAR_UPPERCASE_CODE = 65;
	private static final int CHAR_LOWERCASE_CODE = 97;
	private static final int INT_LENGTH = 1000000;

	/**
	 * Generates random string array
	 *
	 * @param size - Size of the array
	 * @return - String array casted to generic type T[]
	 */
	public static <T extends Comparable<T>> T[] randomStrArr(final int size) {
		String[] randomArr = new String[size];
		StringBuilder sb = new StringBuilder();
		SecureRandom rand = new SecureRandom();
		
		for (int i = 0; i < randomArr.length; i++) {
			sb.setLength(0);
			
			for(int j = 0; j < STR_LENGTH; j++) {
				int randomNum = rand.nextInt(RAND_RANGE_MAX) + RAND_RANGE_MIN;
				char c = new Character(Character.MIN_VALUE);

				switch (randomNum % RAND_RANGE_MAX) {
					case 0:
						c = (char) (rand.nextInt(CHAR_LENGTH) + CHAR_LOWERCASE_CODE);
						break;
					case 1:
						c =  (char) (rand.nextInt(CHAR_LENGTH) + CHAR_UPPERCASE_CODE);
						break;
					case 2:
						c = (char) (rand.nextInt(NUM_LENGTH) + NUM_CODE);
						break;
				}				
				
				sb.append(c);
			}

			randomArr[i] = sb.toString();
		}		

		return (T[]) randomArr;
	}

	/**
	 * Generates random integer array
	 *
	 * @param size - Size of the array
	 * @return - Integer array casted to generic type T[]
	 */
	public static <T extends Comparable<T>> T[] randomIntArr(final int size) {
		Integer[] randomArr = new Integer[size];
		SecureRandom rand = new SecureRandom();

		for (int i = 0; i < randomArr.length; i++)
			randomArr[i] = rand.nextInt(INT_LENGTH);

		return (T[]) randomArr;
	}

	/**
	 * Prints an array
	 *
	 * @param arrayP - The generic array to be printed
	 */
	public static <T extends Comparable<T>> void printArr(final T[] arrP) {
		System.out.println("--------- ARRAY -------");
		for (int i = 0; i < arrP.length; i++)
			System.out.format("%1$8d : %2$d\n", i, arrP[i]);
		System.out.println("------------------------");
	}
}