import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.lang.Exception;
import java.lang.StringBuilder;
import java.lang.InterruptedException;
import java.net.URLDecoder;
import java.util.Scanner;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

public class Knapsack {
	// class constant
	private static final String ZERO = "0";
	private static final String ONE = "1";

	// class data
	private static int ALG = 1;

	// instance data
	private Item[] items;
	private int capacity;
	private boolean[] carriedItems;
	//private int carriedVal;

	// Item of the knapsack - inner class
	private class Item implements Comparable<Item> {
		private int index;
		private int value;
		private int weight;
		private float valDens; // value density - fraction of value/weight

		public Item(final int index, final int value, final int weight) {
			this.index = index;
			this.value = value;
			this.weight = weight;
			this.valDens = ((float) value)/weight;
		}

		@Override
		public int compareTo(Item otherItem) {
			// descending order of value density
			if (this.valDens < otherItem.valDens)
				return 1;
			else if (this.valDens > otherItem.valDens)
				return -1;			
			return 0;			
		}

		@Override
		public String toString() {
			return String.format("Item %5d\t Value: %7d\t Weight: %7d\n", this.index, this.value, this.weight);
		}
	}

	// Data error exception
	private class DataErrorException extends Exception {
		public DataErrorException() {}

		public DataErrorException(final String message) {
			super(message);
		}
	}

	// BruteForceThread class - inner class
	private class BruteForceThread extends Thread {
		// similar to carriedItems but name it differently
		// to avoid name conflict with class variable
		private boolean[] packedItems;
		private int threadId;
		private int bits;
		private CountDownLatch doneSignal;
		private int packedVal;

		/**
		 * Contructor of BruteForceThread.
		 *
		 * @param numOfThreads - Number of threads running.
		 * @param threadId - Thread index.
		 * @param doneSignal - Signal to notify if all threads have completed taks.
		 */
		public BruteForceThread(final int numOfThreads, final int threadId, final CountDownLatch doneSignal) {
			this.bits = (int) (Math.log(numOfThreads)/Math.log(2));
			this.threadId = threadId;
			this.doneSignal = doneSignal;
			
			// set packedItems to contain blocked spaces
			boolean[] temp = new boolean[items.length];
			for (int i = 0; i < threadId-1; i++) {
				int pos = 0;
				while (pos < temp.length && !(temp[pos] = !temp[pos]))
					pos++;
			}
			
			this.packedItems = Arrays.copyOf(temp, temp.length);
		}

		@Override
		public void run() {
			System.out.format("Thread %1$d starts\n", threadId);
			
			boolean[] trialItems = Arrays.copyOf(packedItems, packedItems.length);
			packedVal = calculateValue(packedItems);
					
			while (hasTrial(trialItems, bits)) {
				int trialWt = 0;
				int trialVal = 0;

				for (int i = 0; i < trialItems.length; i++) {
					if (trialItems[i]) {
						trialWt += items[i].weight;
						trialVal += items[i].value;
					}
				}

				if (trialWt <= capacity && trialVal > packedVal) {
					packedVal = trialVal;
					packedItems = Arrays.copyOf(trialItems, trialItems.length);
				}		
			} 
			
			System.out.format("Thread %1$d finished\n", threadId);
			doneSignal.countDown();		
		}

		/**
		 * Prints a boolean arrays of picked items
		 * under the form of binary string (for debug purpose).
		 *
		 * @param itemsP - Boolean array representing picked items.
		 */
		public void printBinStrThread(boolean[] itemsP) {
			System.out.format("Status - Thread %2d: ", threadId);
			for (int i = 0; i < itemsP.length; i++) {
				if (itemsP[i])
					System.out.print(ONE);
				else
					System.out.print(ZERO);
			}
			System.out.println();
		}
	}

	/**
	 * Constructor of Knapsack
	 */
	public Knapsack() {
		items = null;
		capacity = -1;
		carriedItems = null;
	}

	/**
	 * Reads information of a knapsack from file.
	 * 
	 * @param file - File contains information of the knapsack.
	 * @return - true if read is successful, false otherwise.
	 */
	public boolean readKnapSack(final File file) {
		final int ENTRY = 3;
		boolean readSuccess = false;
		int size;
		Scanner sc = null;

		try {
			sc = new Scanner(file);
			
			// read the number of items in list
			// and create an array to hold the items
			size = (int) Float.parseFloat(sc.nextLine());
			if (size < 0) 
				throw new DataErrorException("File has negative size of the knapsack");
			
			items = new Item[size];
			System.out.println("Size: " + size);


			// read each item in the list
			for (int i = 0; i < items.length; i++) {
				String line = sc.nextLine().trim();
				String[] lineSplit = line.split("\\s+");
				int index, currVal, currWt;

				index = Integer.parseInt(lineSplit[0]);

				if (index != i+1)
					throw new DataErrorException("File has invalid index of an item in the knapsack!");
				
				if (lineSplit.length != ENTRY) {
					throw new DataErrorException("File has more/less data of an item in the knapsack!");
				}

				currVal = Integer.parseInt(lineSplit[1]);
				currWt = Integer.parseInt(lineSplit[2]);

				if (currWt < 0)
					throw new DataErrorException("File has negative weight of an item in the knapsack!");

				items[i] = new Item(index, currVal, currWt);
			}

			// read the capacity of a knapsack
			capacity = Integer.parseInt(sc.nextLine());

			if (capacity < 0)
				throw new DataErrorException("File has negative capacity of the knapsack!");

			if (sc.hasNextLine())
				throw new Exception("File has more data than needed in knapsack!");
			
			// read successful
			readSuccess = true;

			// set boolean carriedItems
			carriedItems = new boolean[items.length];

		} catch (FileNotFoundException fnfE) {
			System.out.format("File \"%s\" not found", file.getName());
		} catch (NumberFormatException nfE) {
			System.out.println("File has unmatched size and number of items in the knapsack OR File has invalid data for number of size or capacity!");
		} catch (DataErrorException derrE) {
			System.out.println(derrE.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (sc != null)
				sc.close();
		}	
	
		return readSuccess;			
	}
	
	/**
	 * Finds an optimal solution for a knapsack
	 * using brute force algorithm.
	 */
	public void bruteForce() {
		final int BLOCKED_SPACE = 0;
		boolean[] trialItems = new boolean[items.length];
		int carriedVal = 0;

		while (hasTrial(trialItems, BLOCKED_SPACE)) {
			int trialWt = 0;
			int trialVal = 0;

			for (int i = 0; i < trialItems.length; i++) {
				if (trialItems[i]) {
					trialWt += items[i].weight;
					trialVal += items[i].value;
				}
			}

			if (trialWt <= capacity && trialVal > carriedVal) {
				carriedVal = trialVal;
				carriedItems = Arrays.copyOf(trialItems, trialItems.length);
			}
		}

	}

	/**
	 * Finds an optimal solution for a knapsack
	 * using brute force algorithm runing multithreading.
	 *
	 */
	public void bruteForceMultiThread() {
		final int BASE = 2;
		//int cores = Runtime.getRuntime().availableProcessors();
		int cores = 4;
		int numOfThreads = (int) Math.pow( BASE, (int) (Math.log(cores)/Math.log(2)) );
		CountDownLatch doneSignal = new CountDownLatch(numOfThreads);

		//System.out.println("Inspect cores: " + cores);
		//System.out.println("Inspect threads: " + numOfThreads);
		
		try {
			BruteForceThread[] threads = new BruteForceThread[numOfThreads];
			int carriedVal = 0;
			
			for (int i = 0; i < numOfThreads; i++) {
				threads[i] = new BruteForceThread(numOfThreads, (i+1), doneSignal);
				threads[i].start();
			}

			doneSignal.await();
			
			for (int i = 0; i < numOfThreads; i++) {
				if (threads[i].packedVal > carriedVal) {
					carriedVal = threads[i].packedVal;
					carriedItems = threads[i].packedItems;
				}
			}
		} catch (InterruptedException interuptdE) {
			System.out.println("Interupted");
		}
	}

	/**
	 * Checks if a boolean array of trial items
	 * still have space to put other items in [0,(array.length-blockedSpace)).
	 *
	 * @param trialItems - Boolean array of trial items to be tested.
	 * @param blockedSpace - Space be blocked in the array.
	 * @return - true if an array still has space, false otherwiswe.
	 */
	private boolean hasTrial(final boolean[] trialItems, final int blockedSpace) {
		// while loop from beginning of array -> speed of bruteforce~700s, multibf~300s (4 threads) (hard33.txt)
		// int pos = 0;
		// while (pos < trialItems.length - blockedSpace && !(trialItems[pos] = !trialItems[pos]))
		// 	pos++;
		// return (pos == trialItems.length - blockedSpace) ? false : true;

		// for loop from beginning of array -> spped of bruteforec~610-650s, multibf~260-300s (4 threads) (hard33.txt)
		// int pos;
		// for (pos = 0; pos < trialItems.length - blockedSpace; pos++) {
		// 	if (trialItems[pos] = !trialItems[pos])
		// 		break;
		// }
		// return (pos == trialItems.length - blockedSpace) ? false : true;
		
		// while loop from end of array -> speed of bruteforce~500s, multibf~250s (4 threads) (hard33.txt)
		// int pos = trialItems.length-1;
		// while (pos >= blockedSpace && !(trialItems[pos] = !trialItems[pos]))
		// 	pos--;
		// return (pos == blockedSpace-1) ? false : true;

		// for loop from end of array -> spped of bruteFore~-500s, multibf~250s (4 threads) (hard33.txt)
		int pos;
		for (pos = trialItems.length-1; pos >= blockedSpace; pos--) {
			if (trialItems[pos] = !trialItems[pos])
				break;
		}
		return (pos == blockedSpace-1) ? false : true;
	}

	/**
	 * Prints a boolean arrays of picked items
	 * under the form of binary string.
	 *
	 * @param itemsP - Boolean array representing picked items .
	 */
	public void printBinStr(final boolean[] itemsP) {
		System.out.print("Status: ");
		for (int i = 0; i < itemsP.length; i++) {
			if (itemsP[i])
				System.out.print(ONE);
			else
				System.out.print(ZERO);
		}
		System.out.println();
	}
	
	/**
	 * Finds a feasible solution for a knapsack
	 * using greedy algorithm.
	 */
	public void greedy() {
		Item[] copiedItems = Arrays.copyOf(items, items.length);
		int trialWt = 0;
		
		Arrays.sort(copiedItems);
		
		/*
		System.out.println("New item after sort");
		for (int i = 0; i < copiedItems.length; i++)
			System.out.format("%7s %7s %7s %8s\n", copiedItems[i].index, copiedItems[i].value, copiedItems[i].weight, copiedItems[i].valDens;
		*/				

		for (int i = 0; i < copiedItems.length; i++) {
			Item pickedItem = copiedItems[i];
			
			if (trialWt + pickedItem.weight <= capacity) {
				trialWt += pickedItem.weight;
				if (!carriedItems[pickedItem.index-1])
					carriedItems[pickedItem.index-1] = true;
			}		

		}
	}

	/**
	 * Finds a feasible solution for a knapsack
	 * using greedy-heuristic algorithm.
	 */
	public void greedyHeuristic() {
		Item[] copiedItems = Arrays.copyOf(items, items.length);
		int idx = 0;
		int carriedVal = 0;

		Arrays.sort(copiedItems);

		do {
			Item pickedItem = items[idx];

			if (pickedItem.weight > capacity)
				continue;
			
			boolean[] trialItems = new boolean[items.length];
			int trialWt, trialVal;

			trialItems[idx] = true;
			trialWt = pickedItem.weight;
			trialVal = pickedItem.value;
			
			for (int i = 0; i < copiedItems.length; i++) {
				if (pickedItem.index != copiedItems[i].index) {
					Item nextItem = copiedItems[i];

					if (trialWt + nextItem.weight <= capacity) {
						trialWt += nextItem.weight;
						trialVal += nextItem.value;
						if (!trialItems[nextItem.index-1])
								trialItems[nextItem.index-1] = true;
					}
				}
			}		
			
			if (trialVal > carriedVal) {
				carriedVal = trialVal;
				carriedItems = Arrays.copyOf(trialItems, trialItems.length);
			}
			
			idx++;
						
		} while (idx < items.length);
	}

	/**
	 * Calculates the total weight of picked items
	 * in a boolean array items.
	 *
	 * @param itemsP - Boolean array representing picked items.
	 * @return - Total weight of the picked items.
	 */
	private int calculateWeight(final boolean[] itemsP) {
		int weight = 0;
		for (int i = 0; i < itemsP.length; i++)
			if (itemsP[i])
				weight += items[i].weight;

		return weight;
	}

	/**
	 * Calculates the total value of picked items
	 * in a boolean array items.
	 *
	 * @param itemsP - Boolean array representing picked items.
	 * @return - Total value of the picked items.
	 */
	private int calculateValue(final boolean[] itemsP) {
		int value = 0;
		for (int i = 0; i < itemsP.length; i++)
			if (itemsP[i])
				value += items[i].value;

		return value;	
	}

	/**
	 * Prints the knapsack.
	 */
	public void printKnapSack()	 {
		StringBuilder sb = new StringBuilder();

		sb.append("The Item list:\n");
		sb.append("Size: " + items.length + "\n");
		for (Item item : items)
			sb.append(item);
		sb.append("Capacity: " + capacity);

		System.out.println(sb.toString());
	}

	/**
	 * Prints the solution of a knapsack.
	 * 
	 * @param msg - Message to be printed along with the result.
	 */
	public void printSolution(final String msg) {
		StringBuilder result = new StringBuilder();
		StringBuilder carriedItemsList = new StringBuilder();
		int carriedItemsCount;
				
		result.append("\n");
		result.append(msg);
		carriedItemsCount = countCarriedItems(carriedItemsList);
		if (carriedItemsCount > 1) {
			result.append("Value: " +  calculateValue(carriedItems) + " ");
			result.append("Weight: " + calculateWeight(carriedItems));
			result.append("\n");
			result.append(carriedItemsList);
		} else {
			result.append("The knapsack cannot carry any item to satisfy the conditions");
		}
		
		System.out.println(result.toString());
	}

	/**
	 * Counts how many items a knapsack carries.
	 *
	 * @param carriedItemsList - List of carried items in the knapsack.
	 * @return - Number of carried items in the knapsack.
	 */
	private int countCarriedItems(final StringBuilder carriedItemsList) {
		int count = 0;
		for (int i = 0; i < carriedItems.length; i++) {
			if (carriedItems[i]) {
				carriedItemsList.append((i+1) + " ");
				count++;
			}
		}

		return count;
	}
	
	public static void main(String[] args) {
		String inFileName = null;
		
		try {
			File tempFile = new File(Knapsack.class.getResource("Knapsack.class").getPath());
			String fileN;
			Scanner sc = new Scanner(System.in);
			boolean readSuccess;
			Knapsack knapsack = new Knapsack();
					
			//System.out.print("Input the file of item list: ");
			//fileN = sc.nextLine();
			//fileN = "easy20.txt";
			//fileN = "hard30.txt";
			fileN = "hard33.txt";
			inFileName = URLDecoder.decode(tempFile.getParent(), "UTF-8") + File.separator +  fileN;

			// set algorithm test
			Knapsack.ALG = 3;

			// read from file
			readSuccess = knapsack.readKnapSack(new File(inFileName));
			
			// check if read file is successful and print results
			if (readSuccess) {
				
				knapsack.printKnapSack();
				
				long tic = System.currentTimeMillis();
				switch (ALG) {
					case 1:
						knapsack.bruteForce();
						break;
					case 2:
						knapsack.bruteForceMultiThread();
						break;
					case 3:
						knapsack.greedy();
						break;
					case 4:
						knapsack.greedyHeuristic();
						break;					
				}				
				long tac = System.currentTimeMillis();
				
				switch (Knapsack.ALG) {
					case 1:
					case 2:
						knapsack.printSolution("Optimal solution found: ");
						break;
					case 3:
					case 4:
						knapsack.printSolution("Feasible solution (not necessarily optimal) found: ");
						break;
				}			
				
				System.out.println("\n" + (tac-tic)/1000 + "s elapsed");
			}		

		} catch (UnsupportedEncodingException ueE) {
			System.out.println("Not supporting encoding");
		}
	}

}