package mydict;

import java.util.ArrayList;


public class MyDictionary_2 {
	// some constants for the class
	private final int MULTIPLIER = 31;
	private final int DEFAULT_TABLE_SIZE = 3;

	// private data of MyDictionary_2 class
	private Object[] dictionary;
	private int dictSize;

	// Element data structure
	private class Element {
		private String key;
		private Object data;

		/**
		 * Constructor of element
		 */
		public Element(String key, Object data) {
			this.key = key;
			this.data = data;
		}
	}

	/**
	 * Constructor of MyDictionary_2
	 * with the default table size
	 */
	public MyDictionary_2() {
		dictSize = nextPrime(DEFAULT_TABLE_SIZE);
		dictionary = new Object[dictSize];
	}

	/**
	 * Constructor of MyDictionary_2
	 *
	 * @param dictSize - Size of the dictionary
	 */
	public MyDictionary_2(int dictSize) {
		dictSize = nextPrime(dictSize);
		dictionary = new Object[dictSize];
	}

	/**
	 * Puts an item into a dictionary
	 *
	 * @param item - Item to be put to dictionary
	 * @param key - Key of an item
	 * @result - Success code of the put operation (1: success, 0: fail)
	 */
	public int put(Object item, String key) {
		int successCode = 0;
		int idx = -1;

		// resize dictionary when it is full
		if (isDictFull())
			resizeDict();

		idx = hash(key);
		// check if the current pos has item
		if (dictionary[idx] != null) {
			// yes, go through the array to find the available pos
			while (dictionary[idx] != null) {
				idx = (idx == dictionary.length-1) ? -1 : idx;
				if (dictionary[++idx] == null) {
					dictionary[idx] = new Element(key, item);
					successCode = 1;
					break;
				}
			}
		} else {
			// no, put item to the current pos
			dictionary[idx] = new Element(key, item);
		}

		return successCode;
	}

	/**
	 * Checks if a dictionary is full
	 *
	 * @return - True if a dictionary is full and False if a dictionary is not full
	 */
	public boolean isDictFull() {
		int count = 0;
		
		for (int i = 0; i < dictionary.length; i++) {
			if (dictionary[i] != null)
				count++;				
		}

		//if (count == dictSize)
		// 	System.out.println("FULL");

		return (count == dictSize);
	}

	/**
	 * Resizes a dictionary
	 *
	 * @return - Success code for resize operation (1: success, 0: fail)
	 */
	private int resizeDict() {
		dictSize = nextPrime(dictionary.length + 1);
		int successCode = 0;
		int i;
		Object[] newDict = new Object[dictSize];

		for (i = 0; i < dictionary.length; i++) {
			// check if there is an item in an old dictionary
			if (dictionary[i] instanceof Element) {
				// yes, copy the item
				Element currentElement = (Element) dictionary[i];
				int idx = hash(currentElement.key);
				// check if there is an item in a new dictionary at current index
				if (newDict[idx] != null) {
					// yes, find the available place to put the item
					while (newDict[idx] != null) {
						idx = (idx == newDict.length-1) ? -1 : idx;
						if (newDict[++idx] == null) {
							newDict[idx] = dictionary[i];
							break;
						}
					}
				} else {
					// no, copy the item
					newDict[idx] = dictionary[i];
				}

			}
		}

		if (i == dictionary.length) {
			dictionary = newDict;
			successCode = 1;
		}

		//System.out.println("Resize Dict Result: " + successCode);
		return successCode;
	}

	/**
	 * Generates a next prime from an input number
	 *
	 * @param n - Input number
	 * @return - Next prime number from n
	 */
	private int nextPrime(int n) {
		boolean foundPrime = true;
		int nextPrime;

		// check first if this is a prime number
		for (int i = 2; i <= n/2; i++) {
			if (n % i == 0) {
				foundPrime = false;
				break;
			}
		}

		if (!foundPrime) {
			/* no, try to find next one */
			nextPrime = n;
			
			while (!foundPrime) {
				nextPrime++;
				foundPrime = true;
				
				for (int i = 2;  i <= nextPrime/2; i++) {
					if (nextPrime % i == 0 ) {
						foundPrime = false;
						break;
					}
				}
			}

			return nextPrime;

		} else {
			/* yes, return this as is */
			return n;
		}
	}

	/**
	 * Gets items from a string key
	 *
	 * @param key - String key of an item
	 * @return - Array of items if found or null if not found
	 */
	public ArrayList<Object> get(String key) {
		int originPos = hash(key);
		ArrayList<Object> foundElements = new ArrayList<>();
				
		if (dictionary[originPos] != null) {
			for (int i = 0; i < dictionary.length; i++) {
				if (dictionary[i] instanceof Element) {
					Element currentElement = (Element) dictionary[i];
					if (currentElement.key.equals(key))
						foundElements.add(currentElement.data);
				}
			}
		}

		return (foundElements.size() > 0) ? foundElements : null;
	}

	/**
	 * Delete an item from a string key
	 *
	 * @param key - String key of an item to be deleted
	 * @return - Success code for delete operation (1: success, 0: fail)
	 */
	public int del(String key) {
		int originPos = hash(key);
		int successCode = 0;

		if (dictionary[originPos] != null) {

			// delete all items matching key
			for (int i = 0; i < dictionary.length; i++) {
				if (dictionary[i] instanceof Element) {
					Element currentElement = (Element) dictionary[i];
					if (currentElement.key.equals(key)) {
						//System.out.println("Key delete " + currentElement.key);
						//System.out.println("Delete at pos" + i);
						dictionary[i] = null;
					}
				}
			}

			// loop through the array find all items with same hash
			// and put to the places of removed items
			for (int i = 0; i < dictionary.length; i++) {
				if (dictionary[i] instanceof Element) {
					Element currentElement = (Element) dictionary[i];
					int hashPos = hash(currentElement.key);

					if (hashPos == i)
						continue;

					if (hashPos == originPos) {
						boolean needArrangement = true;
						int idx = originPos;

						while (needArrangement) {
							if (dictionary[idx] == null) {
								dictionary[idx] = dictionary[i];
								dictionary[i] = null;
								needArrangement = false;
							} else {
								idx = (idx == dictionary.length-1) ? 0 : ++idx;
								if (idx == i)
									needArrangement = false;
							}
						}
					}

				}
			}
		}

		return successCode;
	}

	/**
	 * Generates a hash from a string
	 *
	 * @param key - String to get hash
	 * @return - Hash of the string
	 */
	private int hash(String key) {
		long h = 0;

		for(int i = 0;  i < key.length(); i++)
			h = MULTIPLIER * h + key.charAt(i);

		return (int) Math.abs((h % dictSize));
	}

	/**
	 * Prints a dictionary 
	 */
	public void printDictionary() {
		System.out.println("************");
		System.out.println("ITEM IN DICTIONARY");
		System.out.println("SIZE: " + dictSize);
		for (int i = 0; i < dictionary.length; i++) {
			if (dictionary[i] instanceof Element) {
				Element currentElement = (Element) dictionary[i];
				System.out.format("Index %1$2d Key %2$s %3$s\n", i, currentElement.key, currentElement.data);
			}
		}
		System.out.println("************");
	}

	/**
	 * Get size of a dictionary
	 *
	 * @return - Size of a dictionary
	 */
	public int getDictSize() {
		return dictSize;
	}

	/**
	 * Get index of an item with string key
	 *
	 * @param key - String key of an item
	 * @return - Index of an item in dictionary
	 */
	public int getIndex(String key) {
		long h = 0;

		for(int i = 0;  i < key.length(); i++)
			h = MULTIPLIER * h + key.charAt(i);

		return (int) (h % dictSize);
	}


}