package mydict;

import java.util.Random;
import java.security.SecureRandom;
import java.lang.StringBuilder;
import java.util.ArrayList;

public class TestDictionary {
	// some constants
	private static final int TEST_NUMBER = 15000;
	private static final int DEL_KEY_NUMBER = 3;
	private static final int RAND_RANGE_MAX = 3;
	private static final int RAND_RANGE_MIN = 1;

	// test constants
	private final static int STRLEN = 32;
	private final static int N = 30;
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();

	private static class Album {
		// constant for price
		private final int MAX = 1000000;
		private final int MIN = 5000;		
		private final int CHAR_LENGTH = 26;
		private final int CHAR_UPPERCASE = 65;
		private final int CHAR_LOWERCASE = 97;
		private final int STR_LENGTH_CODE = 3;
		private final int STR_LENGTH_NAME = 5;

		private String code;
		private String name;
		private int cost;
		private Random rand = new Random();


		public Album() {
			this.code = generateString(STR_LENGTH_CODE).toUpperCase();
			this.name = generateString(STR_LENGTH_NAME);
			this.cost = Math.abs(rand.nextInt() * MAX + MIN);
		}

		public Album(String code) {
			this.code = code;
			this.name = generateString(STR_LENGTH_NAME);
			this.cost = Math.abs(rand.nextInt() * MAX + MIN);
		}

		public String getCode() {
			return code;
		}

		private String generateString(int length) {
			StringBuilder sb = new StringBuilder();
			Random rand = new Random();
				
			for(int i = 0; i < length; i++) {
				int randomNum = rand.nextInt(RAND_RANGE_MAX) + RAND_RANGE_MIN;
				char c = (randomNum % 2 == 0) ? (char) (rand.nextInt(CHAR_LENGTH) + CHAR_LOWERCASE) : (char)(rand.nextInt(CHAR_LENGTH) + CHAR_UPPERCASE);
				sb.append(c);
			}			

			return sb.toString();
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();

			sb.append("CODE: "); sb.append(code);
			sb.append("\tName: "); sb.append(name);
			sb.append("\tCost: "); sb.append(cost);
			
			return sb.toString();
		}
	}

	private static void printAlbums(ArrayList<Object> list) {
		if (list != null) {
			if (list.get(0) instanceof Album) {
				Album album = (Album) list.get(0);
				System.out.printf("Found item(s) with key (\"%s\"): %d\n", album.getCode(), list.size());
			}
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i) instanceof Album) {
					Album album = (Album) list.get(i);
					System.out.println((i+1) + ": " + album.toString());
				}
			}
		} else {
			System.out.println("No item found");
		}
	}

	private static String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}
	
	private static void test_1() {
		MyDictionary_1 myDict_1 = new MyDictionary_1();
		ArrayList<Object> albums = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		ArrayList<String> keys = new ArrayList<>();
		int foundKey = 0;
		Random rand = new Random();

		// generate random album
		for (int i = 0; i < TEST_NUMBER; i++) {
			Album album =  new Album();
			if (i > TEST_NUMBER/2 && foundKey < DEL_KEY_NUMBER) {
				int randomNum = rand.nextInt(RAND_RANGE_MAX) + RAND_RANGE_MIN;
				String key = (randomNum % 2 == 0) ? album.getCode() : null;
				
				if (key != null) {
					keys.add(key);
					foundKey++;
				}
			}
			//System.out.println(i + ": " + album.toString());
			myDict_1.put(album, album.getCode());
		}
		
		//myDict_1.printDictionary();
		myDict_1.printMainIndex();
		// print the whole dictionary
		myDict_1.printToFile();
		// remove some keys in the dictionary
		for (String key : keys) {
			myDict_1.del(key);
			sb.append("REMOVE KEY " + key + "\n");			
		}
		myDict_1.printToFile("modifieddict.txt", sb.toString());
		

		// Album al1 = new Album("US");
		// Album al2 = new Album("KI");
		// Album al3 = new Album("SE");
		// Album al4 = new Album("VI");
		// Album al5 = new Album("FI");
		// Album al6 = new Album("TT");
		// Album alE = new Album("TT");
	
		// // System.out.println("******** Before **********");
		// // System.out.println("Size " + myDict_1.getDictSize());
		// // System.out.println("Test " + myDict_1.getIndex(al1.getCode()));
		// // System.out.println("Test " + myDict_1.getIndex(al2.getCode()));
		// // System.out.println("Test " + myDict_1.getIndex(al3.getCode()));

		// myDict_1.put(al1, al1.getCode());
		// myDict_1.put(al2, al2.getCode());
		// myDict_1.put(al3, al3.getCode());
		// //myDict_1.put(alE, alE.getCode());
		// myDict_1.printDictionary();

		// // System.out.println("--- GET ----");
		// // albums = myDict_1.get("TT");
		// // printAlbums(albums);
		// // albums = myDict_1.get("VI");
		// // printAlbums(albums);
		// // System.out.println("-------");

		// System.out.println("******** After **********");
		// myDict_1.put(al4, al4.getCode());
		// myDict_1.put(al5, al5.getCode());
		// myDict_1.put(al6, al6.getCode());
		// System.out.println("Size " + myDict_1.getDictSize());
		// System.out.println("Test " + myDict_1.getIndex(al1.getCode()));
		// System.out.println("Test " + myDict_1.getIndex(al2.getCode()));
		// System.out.println("Test " + myDict_1.getIndex(al3.getCode()));
		// System.out.println("Test " + myDict_1.getIndex(al4.getCode()));
		// System.out.println("Test " + myDict_1.getIndex(al5.getCode()));
		// System.out.println("Test " + myDict_1.getIndex(al6.getCode()));
		// System.out.println("Test " + myDict_1.getIndex(alE.getCode()));

		// myDict_1.printDictionary();
		// System.out.println("--- DELETE ----");
		// myDict_1.del("TT");
		// myDict_1.printDictionary();
	}

	private static void test_2() {
		MyDictionary_2 myDict_2 = new MyDictionary_2();
		ArrayList<Object> albums = new ArrayList<>();
		Album al1 = new Album("US");
		Album al2 = new Album("KI");
		Album al3 = new Album("SE");
		Album al4 = new Album("VI");
		Album al5 = new Album("FI");
		Album al6 = new Album("TT");
		Album alE = new Album("TT");
	
		System.out.println("******** Before **********");
		System.out.println("Size " + myDict_2.getDictSize());
		System.out.println("Test " + myDict_2.getIndex(al1.getCode()));
		System.out.println("Test " + myDict_2.getIndex(al2.getCode()));
		System.out.println("Test " + myDict_2.getIndex(al3.getCode()));

		myDict_2.put(al1, al1.getCode());
		myDict_2.put(al2, al2.getCode());
		myDict_2.put(al3, al3.getCode());
		myDict_2.printDictionary();


		System.out.println("******** After **********");
		myDict_2.put(al4, al4.getCode());
		myDict_2.put(al5, al5.getCode());
		myDict_2.put(al6, al6.getCode());
		myDict_2.put(alE, alE.getCode());
		System.out.println("Size " + myDict_2.getDictSize());
		System.out.println("Test " + myDict_2.getIndex(al1.getCode()));
		System.out.println("Test " + myDict_2.getIndex(al2.getCode()));
		System.out.println("Test " + myDict_2.getIndex(al3.getCode()));
		System.out.println("Test " + myDict_2.getIndex(al4.getCode()));
		System.out.println("Test " + myDict_2.getIndex(al5.getCode()));
		System.out.println("Test " + myDict_2.getIndex(al6.getCode()));
		System.out.println("Test " + myDict_2.getIndex(alE.getCode()));
		myDict_2.printDictionary();
		
		System.out.println("--- GET ----");
		albums = myDict_2.get("TT");
		printAlbums(albums);
		// // albums = myDict_2.get("VI");
		// // printAlbums(albums);
		System.out.println("-------");

		System.out.println("--- DELETE ----");
		myDict_2.del("VI");
		myDict_2.printDictionary();
	}

	private static void test_3() {
		MyDictionary_1 dict = new MyDictionary_1(N/2);
		String[] tableStr;
		String search_element;
		int j = 0;
		Object empty, result;
		int r;

		/* first create collection of strings */
		tableStr = new String[N];
		for (int i = 0; i < tableStr.length; i++)
			tableStr[i] = randomString(STRLEN);
		
		/* then store it to the dictionary */
		empty = new Object();
		for (int i = 0; i < tableStr.length; i++)
			dict.put(empty, tableStr[i]);
		System.out.println("Content of the dictionary:");
		dict.printDictionary();

		/* try to search half of the strings from the dictionary */
		System.out.println("\n\nHalf of the searches should succeed, half fail:");
		for (int i = 0; i < N/2; i++) {
			j = rnd.nextInt(N);
			search_element = tableStr[j];
			if (i > N/4) search_element += "#"; // quater of the strings should not be found
			result = dict.get(search_element);
			System.out.format("%1$2d: element %2$s (%3$02d), search result %4$s\n", i, search_element, j, result!=null ? "F" : "N");
		}

		/* then delete first and the middle element from the dictionary */
		dict.del(tableStr[0]);
		dict.del(tableStr[N/2-1]);

		/* try to search again strings from the dictionary, first and the last should not be found */
		System.out.println("\n\nFirst and last search should fail, other should succeed:");
		for (int i = 0; i < N/2; i++) {
			j = i;
			search_element = tableStr[i];
			result = dict.get(search_element);
			System.out.format("%1$2d: element %2$s (%3$02d), search result %4$s\n", i, search_element, j, result!=null ? "F" : "N");
		}
	}

	private static void test_4() {
		MyDictionary_2 dict = new MyDictionary_2(N/2);
		String[] tableStr;
		String search_element;
		int j = 0;
		Object empty, result;
		int r;

		/* first create sorted collection of strings */
		tableStr = new String[N];
		for (int i = 0; i < tableStr.length; i++)
			tableStr[i] = randomString(STRLEN);
		
		/* then store it to the dictionary */
		empty = new Object();
		for (int i = 0; i < tableStr.length; i++)
			dict.put(empty, tableStr[i]);
		System.out.println("Content of the dictionary:");
		dict.printDictionary();

		/* try to search half of the strings from the dictionary */
		System.out.println("\n\nHalf of the searches should succeed, half fail:");
		for (int i = 0; i < N/2; i++) {
			j = rnd.nextInt(N);
			search_element = tableStr[j];
			if (i > N/4) search_element += "#"; // quater of the strings should not be found
			result = dict.get(search_element);
			System.out.format("%1$2d: element %2$s (%3$02d), search result %4$s\n", i, search_element, j, (result!=null) ? "F" : "N");
		}

		/* then delete first and the middle element from the dictionary */
		dict.del(tableStr[0]);
		dict.del(tableStr[N/2-1]);
		dict.printDictionary();

		/* try to search again strings from the dictionary, first and the last should not be found */
		System.out.println("\n\nFirst and last search should fail, other should succeed:");
		for (int i = 0; i < N/2; i++) {
			j = i;
			search_element = tableStr[i];
			result = dict.get(search_element);
			System.out.format("%1$2d: element %2$s (%3$02d), search result %4$s\n", i, search_element, j, (result!=null) ? "F" : "N");
		}
	}

	public static void main(String[] args) {

		//test_1();
		//test_2();
		test_3();		
		test_4();
		
	}
}