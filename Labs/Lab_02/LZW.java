package lzw_compression;

import java.net.URLDecoder;
import java.lang.StringBuilder;
import java.util.HashMap;
import java.util.Map;
import java.util.LinkedList;
import java.io.RandomAccessFile;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.EOFException;

public class LZW {
	// some class constants
	private final int  DEFAULT_DICT_SIZE = 256;
	private final int MAX_DICT_SIZE = 4096;
	private final String DICT_COMP_NAME = "compress";
	private final String DICT_DECOMP_NAME = "decompress";
	private final int MAX_BYTE_SEQUENCE = 3;
	private final int FIRST_BYTE_SEQ_DECOMP = 2;
	private final byte[] byteBuffer = new byte[MAX_BYTE_SEQUENCE];

	// private instance data
	private HashMap<String, Integer> dictForComp;
	private HashMap<Integer, String> dictForDecomp;
	private int dictForCompSize;
	private int dictForDecompSize;
	private StringBuilder sb;
	private int charCount;
	private boolean onBuffer;
	private boolean isWritten;

	/**
	 * Constructor of 
	 */
	public LZW() {}

	/**
	 * Prints dictionary
	 *
	 * @param whichDict - Name of dictionary to be printed ("compress" or "decompress")
	 */
	private void printDict(final String whichDict) {
		if (whichDict.equalsIgnoreCase(DICT_COMP_NAME)) {
			for (Map.Entry<String, Integer> entry : dictForComp.entrySet())
				System.out.println(entry.getValue() + ": " + entry.getKey());
		} else if (whichDict.equalsIgnoreCase(DICT_DECOMP_NAME)) {
			for (Map.Entry<Integer, String> entry : dictForDecomp.entrySet())
				System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}
	
    /**
	 * Compresses a file to a new file with .lzw format extension
	 *
	 * @param uncompressedFile - File to be compressed
	 * @throws IOException
	 */
	public void compress(final String uncompressedFile) throws IOException {
		// set class variables
		dictForComp = buildDictForCompression();
		sb = new StringBuilder();
		charCount = 0;
		onBuffer = false;

		// method variables
		File tempFile = new File(LZW.class.getResource("LZW.class").getPath());
		String inFileName = URLDecoder.decode(tempFile.getParent(), "UTF-8") + File.separator +  uncompressedFile;
		String outFileName = URLDecoder.decode(tempFile.getParent(), "UTF-8") + File.separator +  uncompressedFile.replaceAll("(\\S*)\\.\\S+", "$1").concat(".lzw");
		File inFile = new File(inFileName);
		File outFile = new File(outFileName);		
		RandomAccessFile read = new RandomAccessFile(inFile, "r");
		RandomAccessFile out = new RandomAccessFile(outFile, "rw");
		boolean isEOF = false;

		try {
			readToCompress(read);

			while (true) {
				char ch = readToCompress(read);
				
				// if the dictionary for compression does
				// not have a string, add the string to
				// dictionary for compression if a dictionary
				// still has space and set string builder to hold
				// current character, other wise throw exception
				// for full dictionary
				if (!dictForComp.containsKey(sb.toString())) {
					System.out.println("Add string - " + sb.toString());
					System.out.println("Current char " + ch);
					if (dictForCompSize < MAX_DICT_SIZE)
						dictForComp.put(sb.toString(), dictForCompSize++);
					else 
						throw new Exception("The dictionary for compression is FULL!");
					
					//sb = writeToFileComp(sb, out, isEOF);
					writeToFileComp(sb, out, isEOF);
					sb.setLength(0);
					sb.append(ch);
					System.out.println("String after reset " + sb.toString());
				}
				
			}
		
		} catch (EOFException eofE) {
			System.out.println("Insepct onBuffer " + onBuffer);
			// when reach end of file, a byteBuffer
			// may still has some bytes left need to be
			// written to file. So perform once more
			// time the writeToFileComp(dictForCompIdx, out)
			// method to avoid losing bit
			isEOF = true;
			writeToFileComp(sb, out, isEOF);
			
			// close file reading and writing
			read.close();
			out.close();

			System.out.println("Total characters number: " + charCount);
			System.out.println("End of file reached");
		} catch (IOException ioE) {
			System.out.println(ioE);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Builds dictionary for compression
	 * if the dictionary has not been built
	 *
	 * @return - Dictionary for compression
	 */
	private HashMap<String, Integer> buildDictForCompression() {
		if (dictForComp == null) {
			dictForComp = new HashMap<>();
			// build dictionary for compression
			for (int i = 0; i < DEFAULT_DICT_SIZE; i++)
				dictForComp.put(Character.toString((char) i), i);
			
			dictForCompSize = dictForComp.size();
		}

		return dictForComp;
	}

	/**
     * Reads one byte of data from file to compress
     *
     * @param read - File handle to read a file
     * @return - Current character to be read
     */
    private char readToCompress(final RandomAccessFile read) throws IOException {
    	// must read the first byte
		Byte inputByte = new Byte(read.readByte());
		int intVal = inputByte.intValue();
		char ch;

		//String s1 = String.format("%8s", Integer.toBinaryString((byte)inputByte & 0xFF));
		//System.out.println(s1);
		//System.out.println("Input Byte: " + inputByte);
			
		// if the character has value more than 7 bits 
		// then convert its value to 8 bits
		// only works with ANSI encoding
		intVal = (intVal < 0) ? (intVal + DEFAULT_DICT_SIZE) : intVal;
		ch = (char) intVal;
		sb.append(ch);
		charCount++;

		return ch;
    }

    /**
     * Writes and compressed data (3 bytes) to file
     *
     * @param sb - String builder of read data
     * @param out - File handle to write to a file
     * @throws IOException
     */
    private StringBuilder writeToFileComp(final StringBuilder sb, final RandomAccessFile out, final boolean isEOF) throws IOException {
		int dictForCompIdx = -1;
				
		if (charCount == 1) {
			out.writeBytes(sb.toString());
		}
		
		if (sb.length() > 1)
			dictForCompIdx = dictForComp.get(sb.substring(0, sb.length()-1));
		else
			dictForCompIdx = dictForComp.get(sb.toString());
		
    	// if not EOF
    	if (!isEOF) {
			// check if there is no byte buffered in byteBuffer
	    	if (!onBuffer) {
	    		System.out.println("Inspect sb: " + sb.toString());
	    		System.out.println("index " + dictForCompIdx);
	    		// no byte buffered, shift the binary number of the character 4bits
	    		// to the right and assign to the byteBuffer[0], then mask the binary
	    		// number of the character with OxF to keep 4bits low and shift 4bits
	    		// to the left and assign to the byteBuffer[1] 
				byteBuffer[0] = (byte) (dictForCompIdx >> 4);
				byteBuffer[1] = (byte) ((dictForCompIdx & 0xF) << 4);

			} else {
				System.out.println("Inspect sb (3rd): " + sb.toString());
	    		System.out.println("index " + dictForCompIdx);
	    		System.out.println("inspect byteBuffer[1] - " + byteBuffer[1]);
				// yes byte buffered exists, shift the binary number of the character
				// 8bits to the right and add it to the byteBuffer[1], then mask the
				// binary number of the character with 0xFF to keep all 8bits low and
				// assign to byteBuffer[2]
				byteBuffer[1] += (byte) (dictForCompIdx >> 8);
				byteBuffer[2] = (byte) (dictForCompIdx & 0xFF);

				// write byteBuffer to a file and clear byteBuffer
				System.out.println("---------");
				System.out.println("Write");			
				for (int i = 0; i < byteBuffer.length; i++) {
					System.out.println(byteBuffer[i]);
					out.writeByte(byteBuffer[i]);
					byteBuffer[i] = 0;
				}
				System.out.println("---------");
			}
    	}

		// if EOF caught
		if (isEOF) {
			dictForCompIdx = dictForComp.get(sb.toString());
			byteBuffer[0] = (byte) (dictForCompIdx >> 4);
			byteBuffer[1] = (byte) ((dictForCompIdx & 0xF) << 4);
						
			out.writeByte(byteBuffer[0]);
			out.writeByte(byteBuffer[1]);
			System.out.println("Here");
			System.out.println(byteBuffer[0]);
			System.out.println(byteBuffer[1]);
		}

		// flip byte buffer status
		onBuffer = !onBuffer;

		return sb;
    }

    /**
	 * Decompresses a file to a new file with _decompressed.lzw format extension
	 *
	 * @param compressedFile - File to be decompressed
	 * @throws IOException
	 */
    public void decompress(final String compressedFile) throws IOException {
    	// set class variables
    	dictForDecomp = buildDictForDecompression();
		charCount = 0;
    	onBuffer = false;
    	isWritten = false;
    	
    	// method variables
    	File tempFile = new File(LZW.class.getResource("LZW.class").getPath());
		String inFileName = URLDecoder.decode(tempFile.getParent(), "UTF-8") + File.separator +  compressedFile;
		String outFileName = URLDecoder.decode(tempFile.getParent(), "UTF-8") + File.separator +  compressedFile.replaceAll("(\\S*)\\.\\S+", "$1").concat("_decompressed.lzw");
		File inFile = new File(inFileName);
		File outFile = new File(outFileName);
		RandomAccessFile read = new RandomAccessFile(inFile, "r");
		RandomAccessFile out = new RandomAccessFile(outFile, "rw");
		LinkedList<String> refEntries = new LinkedList<>();
		String newWord;

		try {
			readToDecompress(read);
			//refEntries = writeToFileDecomp(refEntries, out);
			writeToFileDecomp(refEntries, out);

			while (true) {
				readToDecompress(read);
				//refEntries = writeToFileDecomp(refEntries, out);
				writeToFileDecomp(refEntries, out);
				newWord = String.join("", refEntries.getFirst(), refEntries.getLast().substring(0,1));

				System.out.println("new word: " + newWord);

				// if the dictionary of decompression does not
				// have a new word add a neword to the dictionary
				if (!dictForDecomp.containsValue(newWord)) {
					
					if (dictForDecompSize < MAX_DICT_SIZE) {
						dictForDecomp.put(dictForDecompSize++, newWord);
						refEntries.remove();
					} else
						throw new Exception("The dictionary for decompression is FULL!");
				}

			}

		} catch (EOFException eofE) {
			// close file reading and writing
			read.close();
			out.close();

			System.out.println("Total characters number: " + charCount);
			System.out.println("End of file reached");
		} catch (IOException ioE) {
			System.out.println(ioE);
		} catch (Exception e) {
			System.out.println(e);
		}
    }

    /**
	 * Builds dictionary for decompression
	 * if the dictionary has not been built
	 *
	 * @return - Dictionary for decompression
	 */
	private HashMap<Integer, String> buildDictForDecompression() {
		if (dictForDecomp == null) {
			dictForDecomp = new HashMap<>();
			// build dictionary for compression
			for (int i = 0; i < DEFAULT_DICT_SIZE; i++)
				dictForDecomp.put(i, Character.toString((char) i));
			
			dictForDecompSize = dictForDecomp.size();
		}

		return dictForDecomp;
	}

	/**
	 * Reads bytes of data to decompress (max 3 bytes)
	 *
	 * @param read - File handle to read a file
	 */
	private void readToDecompress(final RandomAccessFile read) throws IOException {
		if (!onBuffer) {
			// read the first two bytes to byteBuffer[]
			for (int i = 0; i < FIRST_BYTE_SEQ_DECOMP; i++)
				byteBuffer[i] = read.readByte();
			
			onBuffer = true;			
		} else {
			// read the third byte to byteBuffer[]
			byteBuffer[2] = read.readByte();
		}
	}

	/**
	 * Write data from compressed file to a new file with decompression
	 * LZW algorithm
	 *
	 * @param refEntries - Reference entry to hold last and current entry read from file
	 * @param out - File handle to write data to file
	 * @throws IOException 
	 */
	private void writeToFileDecomp(final LinkedList<String> refEntries, final RandomAccessFile out) throws IOException {
		int dictForDecompIdx;

		if (!isWritten) {			
			//System.out.println("byteBuffer[0] - " + (byteBuffer[0] << 4));
			//System.out.println("byteBuffer[1] - " + ((byteBuffer[1] >> 4) & 0x0F));

			// shift the first byte 4bits to the left
			// shift the second byte 4 bit to the right
			// and mask it with 0x0F to clear out singed
			// bits add the result together to get an
			// index of the decompress dictionary
			dictForDecompIdx = (byteBuffer[0] << 4) + ((byteBuffer[1] >> 4) & 0x0F);

			// look up an entry in a dictionary for
			// decompression and write the entry to file
			// then append the entry to a string builder
			if (dictForDecomp.containsKey(dictForDecompIdx)) {
				String entry = dictForDecomp.get(dictForDecompIdx);
				System.out.println("Decompress index (1st & 2nd): " + dictForDecompIdx);
				System.out.println("Entry: " + entry);
				out.writeBytes(entry);
				charCount += entry.length();
				refEntries.add(entry);
			}

		} else {			
			// mask the second byte with 0x0F to keep 4bits low
			// and clear out signed bits then shifts it 8bits
			// to the left, mask the second byte with 0xFF
			// to keep 16bits low and clear out signed bits
			// and then add them together to get
			// an index of the decompress dictionary
			dictForDecompIdx = ((byteBuffer[1] & 0x0F) << 8) + (byteBuffer[2] & 0xFF);
			
			System.out.println("*******");
			System.out.println("Byte Buffer for Decompress:");
			//insepct byteBuffer
			for (int i = 0; i < byteBuffer.length; i++)
				System.out.println(byteBuffer[i]);

			System.out.println("********");
			

			// reset byteBuffer[]
			for (int i = 0; i < byteBuffer.length; i++)
				byteBuffer[i] = 0;
					
			onBuffer = false;

			//System.out.println("Check - " + dictForDecompIdx);
			//System.out.println("dictForDecompSize - " + dictForDecompSize);
			//System.out.println("original size - " + dictForDecomp.size());
			
			// if an index is more than a dictionary's size
			// add new word to a dictionary
			if (dictForDecompIdx > dictForDecomp.size()-1) {
				System.out.println("Here");
				String newWord = String.join("", refEntries.getLast(), refEntries.getLast().substring(0,1));
				dictForDecomp.put(dictForDecompSize++, newWord);
			} 
			
			// look up an entry in a dictionary for
			// decompression and write the entry to file
			// then append the entry to a string builder
			if (dictForDecomp.containsKey(dictForDecompIdx)) {
					String entry = dictForDecomp.get(dictForDecompIdx);
					System.out.println("Decompress index (2nd & 3rd): " + dictForDecompIdx);
					System.out.println("Entry: " + entry);
					out.writeBytes(entry);
					charCount += entry.length();
					refEntries.add(entry);
			}			
		}
		
		isWritten = !isWritten;

		//return refEntries;
	}

	public static void main(String[] args) {
		LZW lzw = new LZW();
				
		try {
			System.out.println("------- COMPRESSION --------");
			lzw.compress("input.txt");
			//lzw.compress("input_test.txt");
			System.out.println("------- DECOMPRESSION --------");
			lzw.decompress("input.lzw");
			//lzw.decompress("input_test.lzw");
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException ioE) {
			System.out.println(ioE);
		}
	}

}