package mydict;

import java.lang.StringBuilder;
import java.io.PrintWriter;
import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;

public class MyDictionary_1 {
	// Some constants for the class
	private final int MULTIPLIER = 31;
	private final int DEFAULT_TABLE_SIZE = 3;
	private final String DEFAULT_FILE_NAME = "dictionary.txt";

	// private data of a MyDictionary_1 class
	private Object[] dictionary;
	private int dictSize;
	
	// Node of the linked list data structure
	private class Node {
		private String key;
		private Object item;
		private Node next;

		/**
		 * Constructor of Node
		 *
		 * @param item - Item contained in a node
		 * @param key - Key of a item
		 */
		public Node(Object item, String key) {
			this.item = item;
			this.key = key;
			this.next = null;
		}

		/**
		 * Constructor of a node
		 *
		 * @param node - Node to be copied
		 */
		public Node(Node node) {
			this.item = node.item;
			this.key = node.key;
			this.next = null;
		}
	}

	// Linked List data structure
	private class LinkedList {
		private Node first;
		private Node last;

		/**
		 * Constructor of LinkedList
		 */
		public LinkedList() {
			first = null;
			last = null;
		}

		/**
		 * Add an item to a linked list
		 * 
		 * @param item - Item to be added
		 * @param key - Key of the added item
		 */
		public void add(Object item, String key) {
			if (first != null) {
	            Node prev = last;
	            last = new Node(item, key);
	            prev.next = last;
	        }
	        else {
	            last = new Node(item, key);
	            first = last;
	        }
		}

		/**
		 * Print a linked list
		 */

		public void print() {
			Node temp = first;
			while (temp.next != null) {
				System.out.println(temp.item);
				temp = temp.next;
			}
			System.out.println(last.item);
		}

		/**
		 * Delete a node of a linked list
		 *
		 * @param node - Node to be deleted
		 * @return - Success code for the delete operation (1: success, 0: fail)
		 */
		public int delete(Node node) {
			int successCode = 0;

			if (first == node) {
				if (first == last) {
					// remove the only node when
					// a list has one element
					first = first.next;
					last = first;
					successCode = 1;
				} else {
					// remove the first node
					first = first.next;
					successCode = 1;
				}
			} else {
				Node temp = first;
				while (temp != null) {
					if (temp.next == node) {
						
						if (node == last) {
							// update the last node
							// if the removed node is last node
							last = temp;
						}
						
						// remove the middle node
						temp.next = node.next;
						node.next = null;
						successCode = 1;
						break;
					}

					temp = temp.next;
				}
			}

			return successCode;
		}
	}

	/**
	 * Constructor of MyDictionary_1
	 * with the default table size
	 */
	public MyDictionary_1() {
		dictSize = nextPrime(DEFAULT_TABLE_SIZE);
		dictionary = new Object[dictSize];
	}

	/**
	 * Constructor of MyDictionary_1
	 *
	 * @param dictSize - Size of the dictionary
	 */
	public MyDictionary_1(int dictSize) {
		dictSize = nextPrime(dictSize);
		dictionary = new Object[dictSize];
	}

	/**
	 * Puts an item into a dictionary
	 *
	 * @param item - Item to be put to dictionary
	 * @param key - Key of an item
	 * @result - Success code of the put operation (1: success, 0: fail)
	 */
	public int put(Object item, String key) {
		int successCode = 0;
		int idx = -1;
		
		// if a dictionary full resize the dictionary
		if(isDictFull())
			resizeDict();
		
		idx = hash(key);
		// check if there is a linked list at index
		if (dictionary[idx] != null) {
			// yes, add the new item to the linked list
			LinkedList list = null;	
			
			if (dictionary[idx] instanceof LinkedList)
				list = (LinkedList) dictionary[idx];
			
			list.add(item, key);
			successCode = 1;

		} else {
			// no, create  a linked list to hold the item
			LinkedList list = new LinkedList();
			list.add(item, key);
			dictionary[idx] = list;
			successCode = 1;
		}

		return successCode;
	}

	/**
	 * Checks if a dictionary is full
	 *
	 * @return - True if a dictionary is full and False if a dictionary is not full
	 */
	public boolean isDictFull() {
		int count = 0;
		
		for (int i = 0; i < dictionary.length; i++) {
			if (dictionary[i] != null)
				count++;				
		}

		// if (count == dictSize)
		// 	System.out.println("FULL");

		return (count == dictSize);
	}

	/**
	 * Resizes a dictionary
	 *
	 * @return - Success code for resize operation (1: success, 0: fail)
	 */
	private int resizeDict() {
		dictSize = nextPrime(dictionary.length + 1);
		int successCode = 0;
		//System.out.println("New size " + dictSize);
		Object[] newDictionary = new Object[dictSize];

		// copy elements from the old table to the new table
		for (int i = 0; i < dictionary.length;  i++) {
			if (dictionary[i] instanceof LinkedList) {
				LinkedList list = (LinkedList) dictionary[i];
				Node currentNode = list.first;
				
				while (currentNode != null) {
					Node newNode = new Node(currentNode);
					copyNode(newNode, newDictionary);
					currentNode = currentNode.next;
				}
			}

			if (i == dictionary.length -1)
				successCode = 1;
		}

		// System.out.println("*** OLD TABLE *****");
		// System.out.println("Size: " + dictionary.length);
		// for (int i = 0; i < dictionary.length;  i++) {
		// 	if (dictionary[i] instanceof LinkedList) {
		// 		LinkedList list = (LinkedList) dictionary[i];
		// 		System.out.println("Index " + i + ":");
		// 		list.print();
		// 	}
		// }
		// System.out.println("**********");

		// System.out.println("***** NEW TABLE ****");
		// System.out.println("Size: " + newDictionary.length);
		// for (int i = 0; i < newDictionary.length; i++) {
		// 	if (newDictionary[i] instanceof LinkedList) {
		// 		LinkedList list = (LinkedList) newDictionary[i];
		// 		System.out.println("Index " + i + ":");
		// 		list.print();
		// 	}
		// 	else if (newDictionary[i] == null)
		// 		continue;
			
		// }

		dictionary = newDictionary;

		return successCode;
	}

	/**
	 * Generates a next prime from an input number
	 *
	 * @param n - Input number
	 * @return - Next prime number from n
	 */
	private int nextPrime(int n) {
		boolean foundPrime = true;
		int nextPrime;

		// check first if this is a prime number
		for (int i = 2; i <= n/2; i++) {
			if (n % i == 0) {
				foundPrime = false;
				break;
			}
		}

		if (!foundPrime) {
			/* no, try to find next one */
			nextPrime = n;
			
			while (!foundPrime) {
				nextPrime++;
				foundPrime = true;
				
				for (int i = 2;  i <= nextPrime/2; i++) {
					if (nextPrime % i == 0 ) {
						foundPrime = false;
						break;
					}
				}
			}

			return nextPrime;

		} else {
			/* yes, return this as is */
			return n;
		}
	}

	/**
	 * Copies a node from an old dictionary
	 * to the new dictionary
	 *
	 * @param node - Node to be copied
	 * @param newDictionary - New dictionary a node will be copied to
	 */
	private void copyNode(Node node, Object[] newDictionary) {
		int newHash = hash(node.key);

		if (newDictionary[newHash] != null) {
			if (newDictionary[newHash] instanceof LinkedList) {
				LinkedList list = (LinkedList) newDictionary[newHash];
				list.add(node.item, node.key);				
			}
			
		} else  {
			LinkedList list = new LinkedList();
			list.add(node.item, node.key);
			newDictionary[newHash] = list;
		}
	}

	/**
	 * Gets items from a string key
	 *
	 * @param key - String key of an item
	 * @return - Array of items if found or null if not found
	 */
	public ArrayList<Object> get(String key) {
		int idx = hash(key);
		ArrayList<Object> foundItems = new ArrayList<>();
		
		if (dictionary[idx] instanceof LinkedList) {
			LinkedList list = (LinkedList) dictionary[idx];
			Node currentNode = list.first;
				
			while (currentNode != null) {
				if (currentNode.key.equals(key)) {
					foundItems.add(currentNode.item);
				}
				currentNode = currentNode.next;
			}
		}

		return (foundItems.size() > 0) ? foundItems : null;		
	}
	
	/**
	 * Delete an item from a string key
	 *
	 * @param key - String key of an item to be deleted
	 * @return - Success code for delete operation (1: success, 0: fail)
	 */
	public int del(String key) {
		int successCode = 0;
		int idx = hash(key);

		if (dictionary[idx] != null) {
			// yes, return the first object of the list
			if (dictionary[idx] instanceof LinkedList) {
				LinkedList list = (LinkedList) dictionary[idx];
				Node currentNode = list.first;
				
				while (currentNode != null) {
					if (currentNode.key.equals(key)) {
						list.delete(currentNode);
						currentNode = list.first;
					} else {
						currentNode = currentNode.next;
					}
				}

				successCode = 1;
			}					
		}
				
		return successCode;
	}

	/**
	 * Generates a hash from a string
	 *
	 * @param key - String to get hash
	 * @return - Hash of the string
	 */
	private int hash(String key) {
		long h = 0;

		for(int i = 0;  i < key.length(); i++)
			h = MULTIPLIER * h + key.charAt(i);

		return (int) Math.abs((h % dictSize));
	}

	/**
	 * Prints a dictionary 
	 */
	public void printDictionary() {
		System.out.println("************");
		System.out.println("ITEM IN DICTIONARY");
		System.out.println("SIZE: " + dictSize);
		for (int i = 0; i < dictionary.length; i++) {
			if (dictionary[i] instanceof LinkedList) {
				LinkedList list = (LinkedList) dictionary[i];
				System.out.println("Index " + i + ":");
				list.print();
			}
		}
		System.out.println("************");
	}

	/**
	 * Prints main indexes of a dictionary
	 */
	public void printMainIndex() {
		System.out.println("************");
		System.out.println("MAIN INDEX IN DICTIONARY");
		System.out.println("SIZE: " + dictSize);
		for (int i = 0; i < dictionary.length; i++) {
			if (dictionary[i] instanceof LinkedList) {
				LinkedList list = (LinkedList) dictionary[i];
				System.out.println("Index " + i + ":\t" + list.first.item.toString());
			}
		}
		System.out.println("************");
	}

	/**
	 * Prints the dictionary to file by default
	 * The default file name is "dictionary.txt"
	 * The default message is empty
	 */
	public void printToFile() {
		printToFile(null, null);		
	}

	/**
	 * Prints the dictionary to file
	 *
	 * @param fileN - Name of a file to which a dictionary will be printed
	 * @param msg - Additional info needed to print to a file
	 */
	public void printToFile(String fileN, String msg) {
		File file = new File(MyDictionary_1.class.getResource("MyDictionary_1.class").getPath());
		String outFile = "";

		try {
			if (fileN != null) {
				// if file is provided
				outFile =  URLDecoder.decode(file.getParent(), "UTF-8") + File.separator +  fileN;
			} else {
				// if file is not provided
				outFile =  URLDecoder.decode(file.getParent(), "UTF-8") + File.separator +  DEFAULT_FILE_NAME;
			}

			PrintWriter writer = new PrintWriter(outFile, "UTF-8");
			int itemInDictCount = 0;
			
			writer.println("********* DICTIONARY *********");
			writer.println("--- SIZE ---" + dictSize);

			if (msg != null) {
				writer.println(msg);
			}
			
			for (int i = 0; i < dictionary.length; i++) {
				
				if (dictionary[i] instanceof LinkedList) {
					LinkedList list = (LinkedList) dictionary[i];
					Node currentNode = list.first;
					int itemInListCount = 0;

					while (currentNode != null) {
						itemInDictCount++;
						itemInListCount++;
						writer.println(constructString(i, itemInListCount, itemInDictCount, currentNode));
						currentNode = currentNode.next;
					}					
				}
			}

			writer.close();

			System.out.println("Done Writing to File");

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Constructs string to print to file
	 *
	 * @param i - Index of an item in dict
	 * @param itemInListCount - Number of an item in a linked list
	 * @param itemInDictCount - Number of an item in a dictionary
	 * @param node - Node holds an item
	 * @result - String to be printed to file
	 */	
	private String constructString(int i, int itemInListCount, int itemInDictCount, Node node) {
		final int MAIN_INDEX;
		StringBuilder result = new StringBuilder();
		
		if (itemInListCount > 1)
			result.append("\t");
		result.append(i);
		result.append(".");
		result.append(itemInListCount);
		result.append("\t\t");
		result.append(node.item);
		result.append("\t\t\titemCount: (" + itemInDictCount + ")");
		return result.toString();
	}

	/**
	 * Get size of a dictionary
	 *
	 * @return - Size of a dictionary
	 */
	public int getDictSize() {
		return dictSize;
	}

	/**
	 * Get index of an item with string key
	 *
	 * @param key - String key of an item
	 * @return - Index of an item in dictionary
	 */ 	
	public int getIndex(String key) {
		long h = 0;

		for(int i = 0;  i < key.length(); i++)
			h = MULTIPLIER * h + key.charAt(i);

		return (int) (h % dictSize);
	}
}