
import java.io.File;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.util.Arrays;
import java.lang.StringBuilder;
import java.net.URLDecoder;


public class CollectionSearch {
	// private variable used for nested class
	private static final int STR_LENGTH = 32;
	private static final int RAND_RANGE_MAX = 2;
	private static final int RAND_RANGE_MIN = 1;
	private static final int CHAR_LENGTH = 26;
	private static final int CHAR_UPPERCASE = 65;
	private static final int CHAR_LOWERCASE = 97;
	private static final double PROB = 0.5;
	private static final int LOOK_UP_TABLE_MULTIPLIER = 2;

	public static class CollectionSearch1 implements Stopwatch.Test {
		private String[] table;
		private String[] look_upTable;
		private int look_upIdx;
		private int look_upTableSize;

		CollectionSearch1(final int size) {
			look_upTableSize = LOOK_UP_TABLE_MULTIPLIER * size;
			table = new String[size];
			look_upTable = new String[look_upTableSize];
			
			for (int i = 0; i < table.length; i++) {
				table[i] = randomString();
			}

			for (int i = 0; i < look_upTable.length; i++) {
				if (i < table.length)
					look_upTable[i] = table[i];
				else
					look_upTable[i] = randomString();
			}
		}

		private String randomString() {
			StringBuilder sb = new StringBuilder();
			SecureRandom rand = new SecureRandom();
				
			for(int i = 0; i < STR_LENGTH; i++) {
				int randomNum = rand.nextInt(RAND_RANGE_MAX) + RAND_RANGE_MIN;
				char c = (randomNum % 2 == 0) ? (char) (rand.nextInt(CHAR_LENGTH) + CHAR_LOWERCASE) : (char)(rand.nextInt(CHAR_LENGTH) + CHAR_UPPERCASE);
				sb.append(c);
			}			

			return sb.toString();
		}

		private String generateSearchString() {
			String searchStr;
			SecureRandom rand = new SecureRandom();
			double randomNum = rand.nextDouble();
			
			if (randomNum > PROB) {
				searchStr = randomString();
			} else {
				int randomIdx = rand.nextInt(table.length);
				searchStr = table[randomIdx];
			}

			return searchStr;
		}

		public int sequensialSearch(final String e) {

			//System.out.print("Search string: " + e);
			for (int i = 0; i < table.length; i++) {
				if (e.equals(table[i]))
					return i;
			}
			
			return -1;
		}

		public void printCollection() {
			System.out.println("The Collection: " + table.length + " elements");
			for (int i = 0; i < table.length; i++)
				System.out.println(i + ". "+ table[i]);			
		}

		public void printLookUpTable() {
			System.out.println("The Look-up Table: " + look_upTable.length + " elements");
			for (int i = 0; i < look_upTable.length; i++)
				System.out.println(i + ". "+ look_upTable[i]);			
		}

		@Override
		public void generateLookUpIndex() {
			SecureRandom rand = new SecureRandom();
			look_upIdx = rand.nextInt(look_upTableSize);
			//System.out.println("Index: " + look_upIdx);
		}

		@Override
		public void test() {
			int idx = sequensialSearch(look_upTable[look_upIdx]);			
			
			/*
			if (idx > -1) {
				System.out.println("\tFound at pos: " + idx);
			} else {
				System.out.println("\tNot found");
			}
			*/
			
		}

	}

	public static class CollectionSearch2 implements Stopwatch.Test {
		private String[] table;
		private String[] look_upTable;
		private int look_upIdx;
		private int look_upTableSize;

		CollectionSearch2(final int size) {
			look_upTableSize = LOOK_UP_TABLE_MULTIPLIER * size;
			table = new String[size];
			look_upTable = new String[look_upTableSize];
			
			for (int i = 0; i < table.length; i++) {
				table[i] = randomString();
			}

			for (int i = 0; i < look_upTable.length; i++) {
				if (i < table.length)
					look_upTable[i] = table[i];
				else
					look_upTable[i] = randomString();
			}

			Arrays.sort(table);
		}

		private String randomString() {
			StringBuilder sb = new StringBuilder();
			SecureRandom rand = new SecureRandom();
				
			for(int i = 0; i < STR_LENGTH; i++) {
				int randomNum = rand.nextInt(RAND_RANGE_MAX) +RAND_RANGE_MIN;
				char c = (randomNum % 2 == 0) ? (char) (rand.nextInt(CHAR_LENGTH) + CHAR_LOWERCASE) : (char)(rand.nextInt(CHAR_LENGTH) + CHAR_UPPERCASE);
				sb.append(c);
			}			

			return sb.toString();
		}

		private String generateSearchString() {
			String searchStr;
			SecureRandom rand = new SecureRandom();
			double randomNum = rand.nextDouble();
			
			if (randomNum > PROB) {
				searchStr = randomString();
			} else {
				int randomIdx = rand.nextInt(table.length);
				searchStr = table[randomIdx];
			}

			return searchStr;
		}
		
		public int binarySearch(final String e) {
			//System.out.print("Search string: " + e);
			int lowBound = 0;
			int highBound = table.length - 1;

			while (lowBound <= highBound) {
				int idx = (lowBound + highBound)/2;
				if (table[idx].compareTo(e) == 0) {
					return idx;
				} else if (table[idx].compareTo(e) > 0) {
					highBound = idx -1;
				} else {
					lowBound = idx + 1;
				}
			}

			return -1;
		}

		public void printCollection() {
			System.out.println("The Collection:");
			for (int i = 0; i < table.length; i++)
				System.out.println(i + ". "+ table[i]);
		}

		@Override
		public void generateLookUpIndex() {
			SecureRandom rand = new SecureRandom();
			look_upIdx = rand.nextInt(look_upTableSize);
		}

		@Override
		public void test() {
			int idx = binarySearch(look_upTable[look_upIdx]);
			/*
			if (idx > -1) {
				System.out.println("\tFound at pos: " + idx);
			} else {
				System.out.println("\tNot found");
			}
			*/
		}
	}

	private static String getFilePath() {
		String path = "";

		try {
			path =  URLDecoder.decode(file.getParent(), "UTF-8") + File.separator + "data.csv";
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return path;
	}

	// private variable used for main class
	private static final int N = 12; // number of test points
	private static final int STARTN = 1000;
	private static final int INCN = 1000;
	private static final File file = new File(CollectionSearch.class.getResource("CollectionSearch.class").getPath());
	private static final String OUTPUTFILE = getFilePath(); 

	public static void main(String[] args) {
		int arrayN;

		try {
			PrintWriter writer = new PrintWriter(OUTPUTFILE, "UTF-8");
			writer.println("N;N;logN");
			Stopwatch sw = new Stopwatch();
			arrayN = STARTN;
			for (int i = 0; i < N; i++) {
				System.out.println("N: " + arrayN);
				CollectionSearch1 experiment1 = new CollectionSearch1(arrayN);
				CollectionSearch2 experiment2 = new CollectionSearch2(arrayN);
				StringBuilder result = new StringBuilder();

				//experiment1.printCollection();
				//experiment1.printLookUpTable();
				
				sw.configExperiments(50, 20);
				
				result.append(arrayN);

				sw.measure(experiment1);
				result.append(";");
				sw.toValue(result);
								
				sw.measure(experiment2);
				result.append(";");
				sw.toValue(result);
				
				writer.println(result.toString());
				//writer.println(result.toString().replace(".", ","));
	
				arrayN += INCN;
				
			}
			
			writer.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

}