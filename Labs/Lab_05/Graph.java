import java.net.URLDecoder;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.ListIterator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Graph {
	// class constant
	private final static int FROM = 0;
	private final static int TO = 15;

	// instance variable
	private ArrayList<LinkedList<Node>> adjList;

	// Node of the linked list
	private class Node {
		private final int DEFAULT_VERTEX_WEIGHT = -1;
		private int vertexId;
		private int vertexW; // vertex weight

		public Node(int vertexId) {
			this.vertexId = vertexId;
			this.vertexW = DEFAULT_VERTEX_WEIGHT;
		}

		public Node(int vertexId, int vertexW) {
			this.vertexId = vertexId;
			this.vertexW = vertexW;
		}
	}	

	public Graph() {
		adjList = new ArrayList<>();
	}

	/**
	 * Gets number of nodes of a graph
	 *
	 * @return - Number of nodes of a graph
	 */
	public int nodes() {
		return adjList.size();
	}

	/**
	 * Read a graph from file
	 *
	 * @param file - File to read from
	 * @return - true if read successful and false if not
	 * @throws FileNotFoundException
	 */
	public boolean readGraph(final File file) {
		boolean isReadSuccess = false;
		Scanner sc = null;

		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] lineSplit = line.split("\\s");

				if (lineSplit.length > 1) {
					LinkedList<Node> list = new LinkedList();
					
					for (int i = 1; i < lineSplit.length; i++) {
						list.add(new Node(Integer.parseInt(lineSplit[i])));
					}				
					
					adjList.add(list);

				} else if (lineSplit.length == 1) {
					adjList.add(null);
				}
			}

			isReadSuccess = true;
		} catch(FileNotFoundException fnfE) {
			System.out.println("File Not Found!");
		} finally {
			if (sc != null)
				sc.close();
		}
		
		return isReadSuccess;
	}

	/**
	 * Prints graph
	 */
	public void printGraph() {

		for (int i = 0; i < adjList.size(); i++) {			
			
			if (adjList.get(i) instanceof LinkedList) {
				System.out.format("%1$2d:", i);
				LinkedList<Node> list = adjList.get(i);
				ListIterator<Node> listIt = list.listIterator();
				
				while (listIt.hasNext()) {
					Node currNode = listIt.next();

					if (currNode.vertexId != i)
						System.out.format(" %1$d", currNode.vertexId);
				}	

			} else {
				System.out.format("%1$2d: null", i);
			}

			System.out.println();
		}
	}

	/**
	 * Search graph using depth-first-search algorithm
	 * 
	 * @param start - Node from where the search begins
	 * @param visisted - Array of all the visited node
	 * @param pred - Array describes the search path
	 */
	public void dfs(final int start, final boolean[] visited, final int[] prev) {
		visited[start] = true;
		
		if (adjList.get(start) != null && adjList.get(start) instanceof LinkedList) {
			LinkedList<Node> list = adjList.get(start);
			ListIterator<Node> listIt = list.listIterator();

			if (!listIt.hasNext())
				return;

			while (listIt.hasNext()) {
				Node currNode = listIt.next();

				//System.out.println("Inspect current" + currNode.vertexId);
				//System.out.println("Inspect start" + start);
				
				// if current node is already visited
				// go to next node
				if (visited[currNode.vertexId])
					continue;
				else
					prev[currNode.vertexId] = start;

				dfs(currNode.vertexId, visited, prev);
			}
		}
	}

	/**
	 * Checks if a graph has a vertex
	 *
	 * @return - true if a graph has a vertex, false if a graph does not have a vertex
	 */ 
	public boolean hasVertex(final int vertexId) {
		return (adjList.get(vertexId) != null);
	}

	/**
	 * Finds a maze solution
	 * 
	 * @param from - The start vertex
	 * @param to - The end vertex
	 * @param prev - The  
	 */
	private static int mazeSolution(final int from, final int to, final int[] prev, final int[] steps) {
		int i, n, node;
		
		// first count how many edges between the end and the start
		node = to; n = 1;
		while ((node = prev[node]) != from) n++;
		
		// then reverse prev[] array to steps[] array
		node = to; i = n;
		while (i >= 0) {
			steps[i--] = node;
			node = prev[node];
		}
		// include also the end vertex
		return (n+1);
	}

	/**
	 * Inspects contents of two boolean arrays
	 * used to debug array prev and array steps
	 *
	 * @param arr1 - First array to be checked
	 * @param arr2 - Second array to be checked
	 */
	public static void inspectArr(final boolean[] arr1, final int[] arr2) {
		if (arr1 != null) 
			for (int i = 0; i < arr1.length; i++) {
				System.out.println(i + ": " + arr1[i]);
			}

		if (arr2 != null)
			for (int i = 0; i < arr2.length; i++) {
				System.out.println(i + ": " + arr2[i]);
			}
	}
	
	public static void main(String[] args) {
		try {
			File tempFile = new File(Graph.class.getResource("Graph.class").getPath());
			String inFileName = URLDecoder.decode(tempFile.getParent(), "UTF-8") + File.separator +  "maze.grh";
			//String inFileName = URLDecoder.decode(tempFile.getParent(), "UTF-8") + File.separator +  "test.grh";
			Graph graph = new Graph();
			boolean[] visited;
			int[] prev;
			int[] steps;

			boolean success = graph.readGraph(new File(inFileName));
			//System.out.println("Success " + success);
			graph.printGraph();
			visited = new boolean[graph.nodes()];
			prev = new int[graph.nodes()];
			graph.dfs(FROM, visited, prev);

			/*
			System.out.println("Inspect visited");
			inspectArr(visited, null);

			System.out.println("Inspect prev");
			inspectArr(null, prev);
			*/
			
			// then check if there is a solution by looking from the backwards to the start
			
			steps = new int[graph.nodes()];
			System.out.println("\nMaze solution from " + FROM + " to " + TO);;
			int n = mazeSolution(FROM, TO, prev, steps);
			
			for (int i = 0; i < n; i++)
				System.out.print(steps[i] + " ");
			
			System.out.println();

		} catch (IOException ioE) {
			System.out.println(ioE.getMessage());
		}

	}
}