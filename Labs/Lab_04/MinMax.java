package lab04a;

public class MinMax {
	// constant of instance
	private final int DIVISION = 2;
	
	// instance data
	private Comparable[] inArr;
	private int maxIdx;
	private int minIdx;
	private int compCount;

	public MinMax(final Comparable[] arrP) {
		this.inArr = arrP;
		this.minIdx = -1;
		this.maxIdx = -1;
		this.compCount = 0;
	}
	
	public void minmax() {
		int size = inArr.length;
		int start;

		if (size < 1) return;
		else if (size == 1) {
			minIdx = 0;
			maxIdx = 0;
			return;
		}

		start = 1;
		if (size % 2 == 1) {
			minIdx = 0;
			maxIdx = 0;
		} else {
			compCount++;
			if (inArr[0].compareTo(inArr[1]) > 0) {
				minIdx = 1;
				maxIdx = 0;
			} else {
				minIdx = 0;
				maxIdx = 1;
			}
			start = 2;
		}

		for (int i = start; i < size; i+=2) {
			
			compCount++;
			if (inArr[i].compareTo(inArr[i+1]) > 0) {
				compCount++;
				if (inArr[i].compareTo(inArr[maxIdx]) > 0) maxIdx = i;
				compCount++;
				if (inArr[i+1].compareTo(inArr[minIdx]) < 0) minIdx = i+1; 
			} else {
				compCount++;
				if (inArr[i].compareTo(inArr[minIdx]) < 0) minIdx = i;
				compCount++;
				if (inArr[i+1].compareTo(inArr[maxIdx]) > 0) maxIdx = i+1;
			}
		}
	}

	public void minmax2() {
		minIdx = 0;
		maxIdx = 0;

		for (int i = 1; i < inArr.length; i++) {
			
			compCount++;
			if (inArr[i].compareTo(inArr[minIdx]) < 0) {
				minIdx = i;
			}
			
			compCount++;
			if (inArr[i].compareTo(inArr[maxIdx]) > 0) {
				maxIdx = i;
			}
		}
	}

	public int getComparisons() {
		return compCount;
	}

	public int getMax() {
		return maxIdx;
	}

	public int getMin() {
		return minIdx;
	}
}