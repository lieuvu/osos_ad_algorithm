package lab04b;

import java.security.SecureRandom;
import java.lang.StringBuilder;

public class Lab_04b {
	private static final String AB = "0123456789";
	private static SecureRandom rnd = new SecureRandom();
	
	/** 
	 * Creates a random string of a number with lenght of n
	 *
	 * @param n - Length of the the number
	 */
	private static String createTestNumber(int n) {
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int c;
			if (i > 0) // ensure that the first digit is not '0'
				c = rnd.nextInt(AB.length());
			else {
				c = 1 + rnd.nextInt(AB.length()-1);
			}
			sb.append(AB.charAt(c));
		}
		return sb.toString();
	}

	/**
	 * Tests for method mulBigNum
	 */
	private static void test_01() {
		Bignum n1, n2;
		Bignum result = null;
		int n;
		String s1, s2;

		try {
			for (int i = 1; i < 30; i++) {
				s1 = createTestNumber(i);
				n1 = new Bignum(s1);
				System.out.format("%1$30s * ", n1.toString());
				
				s2 = createTestNumber(i);
				n2 = new Bignum(s2);
				System.out.format("%1$30s = ", n2.toString());
				
				result = n1.mulBigNum(n2);
				n = result.rclMulCounter();
				
				System.out.format("%1$60s (%2$d)\n", result, n);
			}
			result.clrMulCounter();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Tests for method mulBigNum2
	 */
	private static void test_02() {
		Bignum n1, n2;
		Bignum result = null;
		int n;
		String s1, s2;

		try {
			for (int i = 1; i < 30; i++) {
				s1 = createTestNumber(i);
				n1 = new Bignum(s1);
				System.out.format("%1$30s * ", n1.toString());
				
				s2 = createTestNumber(i);
				n2 = new Bignum(s2);
				System.out.format("%1$30s = ", n2.toString());
				
				result = n1.mulBigNum2(n2);
				n = result.rclMulCounter();
				
				System.out.format("%1$60s (%2$d)\n", result, n);
			}
			result.clrMulCounter();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Tests for debug purposse
	 */
	private static void test_03() {
		Bignum n1, n2, result;
		int n;

		try {
			n1 = new Bignum("1234");
			n2 = new Bignum("5678");

			result = n1.mulBigNum2(n2);
			System.out.println("--- RESULT ---");
			System.out.format("%1$60s\n", result);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	* @param args the command line arguments
	*/
	public static void main(String[] args) {		
		
		test_01();
		test_02();
		//test_03();
		
	}
}