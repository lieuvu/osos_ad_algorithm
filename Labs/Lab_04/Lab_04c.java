package lab04c;

import java.lang.ArrayIndexOutOfBoundsException;
import java.lang.Exception;
import java.util.Scanner;

public class Lab_04c {
	private static final int POWER_BASE = 2;
	public static void main(String[] args) {
		int inK = -1;
		Scanner sc = null;
		
		try {
			int inDefRow, inDefCol, size;
			DefectChessBoard defBoard; 
			sc = new Scanner(System.in);
			
			System.out.println("Enter, board size is 2^k");
			System.out.println("k should be in the range 0 through 6");
			inK = sc.nextInt();

			if (inK < 0 || inK > 6)
				throw new Exception();
			
			System.out.println("Enter location of defect e.g. row col)");
			inDefRow = sc.nextInt()-1;
			inDefCol = sc.nextInt()-1;

			size = (int) Math.pow(POWER_BASE, inK);
			defBoard = new DefectChessBoard((int) Math.pow(POWER_BASE, inK));
			defBoard.tileBoard(0, 0, inDefRow, inDefCol, size);
			defBoard.printChessBoard();
			defBoard.resetChessBoard();
			
		} catch (ArrayIndexOutOfBoundsException iobE) {
			System.out.println("THE DEFECT IS OUT OF A CHESS BOARD!");
		} catch (Exception e)	 {
			System.out.println("THE BOARD CAN NOT BE CREATED WITH k=" + inK);
		} finally {
			if (sc != null)
				sc.close();
		}
	}
}