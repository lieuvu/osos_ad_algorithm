package lab04b;

import java.util.Arrays;

public class Bignum {
    private byte[] number;          // least significand digit first (index 0), most significand last (index length-1)
    private static int mulCounter;  // variable to count the number of multiplications
    private static final int DIVISION = 2;
    private static final String ZERO = "0";

    /**
     * Constructor of Bignum based on its length
     *
     * @param n - Length of the big number
     */
    public Bignum(final int n) {
        number = new byte[n];
    }
    
    /**
     * Constructor of Bignum based on number string
     *
     * @param s - Number string of a big number
     */
    public Bignum(final String s) {
        int     n = s.length();
        number = new byte[n];

        for (int i = n-1; i >= 0; i--)
            number[n-i-1] = (byte) Character.getNumericValue(s.charAt(i));
    }

    /**
     * create a new number whose digits are x[from, to)
     */
    public Bignum selectBigNum(final int from, final int to) {
        Bignum r = new Bignum(to-from);

        for (int i = from; i < to; i++)
            r.number[i-from] = number[i];

        return r;
    }

    /**
     * Multiplies two number (this * y) together
     * using divide-and-conquer technique, the byte[]
     * data of the Bignum object is mainly utilized
     *
     * @param y - Bignum of factor
     * @throws Exception
     */
    public Bignum mulBigNum(final Bignum y) throws Exception {
		Bignum r, p1, p2, p3;
        Bignum xLeft, xRight, yLeft, yRight;
        Bignum n1, n2;
        int numDigits, leftNumDigits, rightNumDigits, exponent1, exponent2;

        
        // System.out.println("Debug x - " + this);
        // System.out.println("Debug y - " + y);
        if (isZero(this) || isZero(y)) {
            return r = new Bignum(ZERO);

        } else if (number.length == 1 && y.number.length == 1) {
            mulCounter++;
            return r = new Bignum(String.valueOf(number[0] * y.number[0]));

        } else if (isPower10(this) || isPower10(y)) {
            if (isPower10(this)) return power10(y, number.length-1);
            if (isPower10(y)) return power10(this, y.number.length-1);

        } else {
            // make numbers to have same number of digits
            if (number.length  < y.number.length) {
                number = Arrays.copyOf(number, y.number.length);
            }
            else if (number.length > y.number.length)
                y.number = Arrays.copyOf(y.number, number.length);
        }

        numDigits = number.length;
        xLeft = new Bignum(numDigits/DIVISION);
        yLeft = new Bignum(numDigits/DIVISION);
        xRight = new Bignum((int) Math.ceil((double)numDigits/DIVISION));
        yRight = new Bignum((int) Math.ceil((double)numDigits/DIVISION));
        leftNumDigits = xLeft.number.length;
        rightNumDigits = xRight.number.length;

        // System.out.println("numDigits " + numDigits);
        for (int i = numDigits-1; i >= 0; i--) {
            if (leftNumDigits > 0) {
                xLeft.number[leftNumDigits-1] = number[i];
                yLeft.number[leftNumDigits-1] = y.number[i];
                leftNumDigits--;
            } else if (rightNumDigits > 0) {
                xRight.number[rightNumDigits-1] = number[i];
                yRight.number[rightNumDigits-1] = y.number[i];
                rightNumDigits--;
            }
        }

        // System.out.println("Debug xLeft - " + xLeft);
        // System.out.println("Debug xRight - " + xRight);
        // System.out.println("Debug yLeft - " + yLeft);
        // System.out.println("Debug yRight - " + yRight);
        
        p1 = xLeft.mulBigNum(yLeft);
        // System.out.println("Escape p1");
        // System.out.println("Debug p1 - " + p1);
        p2 = xRight.mulBigNum(yRight);
        // System.out.println("Escape p2");
        // System.out.println("Debug p2 - " + p2);
        p3 = xLeft.addBigNum(xRight).mulBigNum(yLeft.addBigNum(yRight));
        // System.out.println("Escape p3");
        // System.out.println("Debug p3 - " + p3);

        // if a number of digits is odd, an exponent
        // equals number of digit plus one, other wise
        // the exponenet equals number of digit
        exponent1 = (numDigits % 2 != 0) ? (numDigits+1) : numDigits;
        exponent2 = (int) exponent1/2;
        n1 = new Bignum(String.valueOf(power10(p1, exponent1)));
        n2 = new Bignum(String.valueOf(power10(p3.subBigNum(p1).subBigNum(p2), exponent2)));

        return r = new Bignum(String.valueOf(n1.addBigNum(n2).addBigNum(p2)));
    }

    /**
     * Multiplies two number (this * y) together
     * using divide-and-conquer technique, manipulation
     * with string is mainly employed
     *
     * @param y - Bignum of factor
     * @throws Exception
     */
    public Bignum mulBigNum2(final Bignum y) throws Exception {
        Bignum r, p1, p2, p3;
        Bignum xLeft, xRight, yLeft, yRight;
        Bignum n1, n2;
        int numDigits, exponent1, exponent2;

        // System.out.println("Debug x - " + this);
        // System.out.println("Debug y - " + y);
        if (isZero(this) || isZero(y)) {
            return r = new Bignum(ZERO);
            
        } else if (number.length == 1 && y.number.length == 1) {
            mulCounter++;
            return r = new Bignum(String.valueOf(number[0] * y.number[0]));

        } else if (isPower10(this) || isPower10(y)) {
            if (isPower10(this)) return power10(y, number.length-1);
            if (isPower10(y)) return power10(this, y.number.length-1);

        } else {
            // make numbers to have same number of digits
            if (number.length  < y.number.length) {
                number = Arrays.copyOf(number, y.number.length);
            }
            else if (number.length > y.number.length)
                y.number = Arrays.copyOf(y.number, number.length);
        }

        numDigits = number.length;
        xLeft = new Bignum(this.toString().substring(0, numDigits/DIVISION));
        yLeft = new Bignum(y.toString().substring(0, numDigits/DIVISION));
        xRight = new Bignum(this.toString().substring(numDigits/DIVISION));
        yRight = new Bignum(y.toString().substring(numDigits/DIVISION));
   
        p1 = xLeft.mulBigNum(yLeft);
        p2 = xRight.mulBigNum(yRight);
        p3 = xLeft.addBigNum(xRight).mulBigNum(yLeft.addBigNum(yRight));

        // if a number of digits is odd, an exponent
        // equals number of digit plus one, other wise
        // the exponenet equals number of digit
        exponent1 = (numDigits % 2 != 0) ? (numDigits+1) : numDigits;
        exponent2 = (int) exponent1/2;
        n1 = new Bignum(String.valueOf(power10(p1, exponent1)));
        n2 = new Bignum(String.valueOf(power10(p3.subBigNum(p1).subBigNum(p2), exponent2)));

        return r = new Bignum(String.valueOf(n1.addBigNum(n2).addBigNum(p2)));
    }

    /**
     * Checks if a Bignum number is zero
     *
     * @param x - Number to be checked
     * @return - true if a number is zero and false if not
     */
    private boolean isZero(final Bignum x) {
        return (x.toString().replaceAll("0", "").length() == 0) ? true : false;
    }

    /**
     * Checks if a Bignum number is power of 10
     *
     * @param x - Number to be checked
     * @return - true if a number is power of 10 and false if not
     */
    private boolean isPower10(final Bignum x) {
        return (x.toString().replaceAll("^10+", "").length() == 0) ? true : false;
    }

    /**
     * Calculates the product of a number and
     * a power of ten (x * 10^exponent)
     *
     * @param x - Number
     * @param exponent - Exponent of a 10 base
     * @return - Product (x * 10^exponent)
     */
    private Bignum power10(final Bignum x, final int exponent) {
        Bignum r;
        StringBuilder rightNumStr = new StringBuilder();
        
        if (isZero(x)) {
            r = new Bignum(ZERO);
        } else {
            // Strip out left zeros
            String rightNumFormat = x.toString().replaceAll("^0+", "");

            // add more zeros to the right
            rightNumStr.append(rightNumFormat);
            for (int i = 0; i < exponent; i++) {
                rightNumStr.append(ZERO);
            }

            r = new Bignum(rightNumStr.toString());
        }

        // System.out.println("Exponent - " + exponent);
        // System.out.println("Inspect r: " + r);         
        
        return r;
    }

    /**
     * Adds two numbers together this + y
     *
     * @param y - Big number to be added to current big number instance
     */
    public Bignum addBigNum(final Bignum y) {
        Bignum r, a, b;
        int    carry;

        // a is the larger number, b is the smaller
        if (number.length > y.number.length) {
            a = this; b = y;
        } else {
            a = y; b = this;
        }

        r = new Bignum(a.number.length);

        // add digits, starting from the least significant digit
        carry = 0;
        for (int i = 0; i < a.number.length; i++) {
            r.number[i] = (byte) (a.number[i] + (i < b.number.length ? b.number[i] : 0) + carry);
            if (r.number[i] > 9) {
                carry = 1;
                r.number[i] -= 10;
            } else
                carry = 0;
        }

        if (carry > 0) {
            r.number = Arrays.copyOf(r.number, r.number.length+1);
            r.number[r.number.length-1] = 1;
        }

        return r;
    }  

    /**
     * Subtracts two numbers this - y
     *
     * @param y - Big number to be substracted from current big number instance
     */
    public Bignum subBigNum(final Bignum y) throws Exception {
        Bignum r = new Bignum(number.length);
        int    carry;

        // sub digits, starting from the least significant digit
        carry = 0;
        for (int i = 0; i < number.length; i++) {
            r.number[i] = (byte)(number[i] - (i < y.number.length ? y.number[i] : 0) - carry);
            if (r.number[i] < 0) {
                carry = 1;
                r.number[i] += 10;
            } else
                carry = 0;
        }   

        if (carry > 0) {
            throw new Exception("Overflow in subtraction\n");
        }

        return r;
    }    

    /**
     * Clears multiplication counter
     */
    public void clrMulCounter() {
        mulCounter = 0;
    }

    /**
     * Gets multiplication counter
     *
     * @return - Multiplication counter
     */
    public int rclMulCounter() {
        return (mulCounter);
    }

    /**
     * Prints out the given number (for debug only)
     */
    public void printBigNum(String s) {
        System.out.println(s + ": " + toString());
    }

    /**
     * Prints out the number to the string s
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        
        for (int i = number.length-1; i >= 0; i--)
            s.append(number[i]);
        
        return (s.toString());
    }
}
