package lab04c;

import java.lang.ArrayIndexOutOfBoundsException;

public class DefectChessBoard {
	// class data
	private static int tileCode = 1;
	private static final int DEFECT_VALUE = 0;
	private static final int DEFAULT_VALUE = -1;
	
	// instance data
	private int[][] chessBoard;
	private int chessBoardSize;

	/**
	 * Constructor of DefectChessBoard
	 *
	 * @param size - Size of a chess board
	 */
	public DefectChessBoard(final int size) {
		this.chessBoardSize = size;
		chessBoard = new int[chessBoardSize][chessBoardSize];
		for (int i = 0; i < chessBoardSize; i++) {
			for (int j = 0; j < chessBoardSize; j++)
				chessBoard[i][j] = DefectChessBoard.DEFAULT_VALUE;
		}
	}

	/**
	 * Tiles a chess board with trinominoes
	 *
	 * @param tr - Coordinate of row of a left-upper point of a board [0,size-1]
	 * @param tr - Coordinate of column of a left-upper point of a board [0,size-1]
	 * @param dr - Coordinate of row of a defect [0,size-1]
	 * @param dc - Cooridnate of column of a defect [0,size-1]
	 * @param size - Size of a row of a chess board
	 */
	public void tileBoard(final int tr, final int tc, final int dr, final int dc, final int size) {
		int qt; //quadrant
		int qtSize = size/2; //quadrant size

		// set the initial defect cell
		if (size == chessBoardSize)
			chessBoard[dr][dc] = DEFECT_VALUE;
		
		if (size == 1) {
			return;
		} else if (size == 2) {
			//printChessBoard();
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					if (chessBoard[i][j] == DEFAULT_VALUE)
						chessBoard[i][j] = tileCode;
				}
			}
		}

		// put triomino in the middle
		qt = tileTriominoInMiddle(tr,tc,dr,dc,size);

		if (qt == 1) {
			// fill quadrant I, II, III, IV
			tileBoard(tr, tc+qtSize, dr, dc, qtSize);
			tileBoard(tr, tc, tr+qtSize-1, tc+qtSize-1, qtSize);
			tileBoard(tr+qtSize, tc, tr+qtSize, tc+qtSize-1, qtSize);	
			tileBoard(tr+qtSize, tc+qtSize, tr+qtSize, tc+qtSize, qtSize);
		} else if (qt == 2) {
			// fill quadrant II, III, IV, I
			tileBoard(tr, tc, dr, dc, qtSize);
			tileBoard(tr+qtSize, tc, tr+qtSize, tc+qtSize-1, qtSize);
			tileBoard(tr+qtSize, tc+qtSize, tr+qtSize, tc+qtSize, qtSize);	
			tileBoard(tr, tc+qtSize, tr+qtSize-1, tc+qtSize, qtSize);
		} else if (qt == 3) {
			// fill quadrant III, IV, I, II
			tileBoard(tr+qtSize, tc, dr, dc, qtSize);
			tileBoard(tr+qtSize, tc+qtSize, tr+qtSize, tc+qtSize, qtSize);
			tileBoard(tr, tc+qtSize, tr+qtSize-1, tc+qtSize, qtSize);	
			tileBoard(tr, tc, tr+qtSize-1, tc+qtSize-1, qtSize);
		} else if (qt == 4) {
			// fill quadrant IV, I; II, III
			tileBoard(tr+qtSize, tc+qtSize, dr, dc, qtSize);
			tileBoard(tr, tc+qtSize, tr+qtSize-1, tc+qtSize, qtSize);
			tileBoard(tr, tc, tr+qtSize-1, tc+qtSize-1, qtSize);	
			tileBoard(tr+qtSize, tc, tr+qtSize, tc+qtSize-1, qtSize);
		}
	}

	/**
	 * Inserts trinomino in the middle of a board
	 *
	 * @param tr - Coordinate of row of the left-upper point of a board [0,size-1]
	 * @param tc - Coordinate of column of the left-upper point of a board [0,size-1]
	 * @param dr - Coordinate of row of a defect [0,size-1]
	 * @param dc - Cooridnate of column of a defect [0,size-1]
	 * @return - Quadrant of a defect point
	 */
	private int tileTriominoInMiddle(final int tr, final int tc, final int dr, final int dc, final int size) {
		int qt = -1; //quadrant
		int qtSize = size/2; // quadrant size
		
		// define a quadrant contain a defect
		if (dr < (tr+qtSize) && dc >= (tc+qtSize)) {
			qt = 1;
			chessBoard[tr+qtSize-1][tc+qtSize-1] = tileCode;
			chessBoard[tr+qtSize][tc+qtSize-1] = tileCode;
			chessBoard[tr+qtSize][tc+qtSize] = tileCode;
		} else if (dr < (tr+qtSize) && dc < (tc+qtSize)) {
			qt = 2;
			chessBoard[tr+qtSize-1][tc+qtSize] = tileCode;
			chessBoard[tr+qtSize][tc+qtSize-1] = tileCode;
			chessBoard[tr+qtSize][tc+qtSize] = tileCode;
		} else if (dr >= (tr+qtSize) && dc < (tc+qtSize)) {
			qt = 3;
			chessBoard[tr+qtSize-1][tc+qtSize-1] = tileCode;
			chessBoard[tr+qtSize-1][tc+qtSize] = tileCode;
			chessBoard[tr+qtSize][tc+qtSize] = tileCode;
		} else {
			qt = 4;
			chessBoard[tr+qtSize-1][tc+qtSize-1] = tileCode;			
			chessBoard[tr+qtSize-1][tc+qtSize] = tileCode;			
			chessBoard[tr+qtSize][tc+qtSize-1] = tileCode;			
		}
		
		tileCode++;

		return qt;
	}

	/**
	 * Prints a chess board
	 */
	public void printChessBoard() {
		System.out.println("---- CHESS BOARD ----\n");
		for (int i = 0; i < chessBoardSize; i++) {
			for (int j= 0; j < chessBoardSize; j++) {
				System.out.format("%1$4d  ", chessBoard[i][j]);
			}
			System.out.println();
		}
		System.out.println("\n--------------");
	}

	/**
	 * Resets a chess board
	 */
	public void resetChessBoard() {
		for (int i = 0; i < chessBoardSize; i++) {
			for (int j = 0; j < chessBoardSize; j++)
				chessBoard[i][j] = DefectChessBoard.DEFAULT_VALUE;
		}
		System.out.println("Board Reset");
	}
}