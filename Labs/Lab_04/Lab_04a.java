package lab04a;

import java.security.SecureRandom;

public class Lab_04a {
	// class constant
	private final static int N = 10;

	// class data
	private static Integer[] collection;
	private static SecureRandom rnd = new SecureRandom();
	
	/** 
	 * Creates a random unsigned value array
	 * with lenght of n
	 *
	 * @param n - Length of the unsigned value array
	 */
	static void initTestSeq(final int n) {
	/* first create the collection of strings */
		collection = new Integer[n];
		for (int i = 0; i < n; i++) {
			collection[i] = rnd.nextInt();
		}
	}
	
	/**
	 * Prints out the sequence
	 */
	static void printTestSeq() {
		for (int i = 0; i < collection.length; i++) {
			System.out.format("%1$02d: %2$11d\n", i, collection[i]);
		}
		System.out.println();
	}
		
	/**
	* @param args the command line arguments
	*/
	public static void main(String[] args) {
		int max, min;
		initTestSeq(N);
		printTestSeq();
		MinMax minmax = new MinMax(collection);
		
		minmax.minmax2();
		System.out.println("Brute Force minmax search");
		System.out.println("Min index " + minmax.getMin() + ", max index " + minmax.getMax());
		System.out.println("Number of comparisions " + minmax.getComparisons());
			
		minmax = new MinMax(collection);
		minmax.minmax();
		System.out.println("Divide and Conquer minmax search");
		System.out.println("Min index " + minmax.getMin() + ", max index " + minmax.getMax());
		System.out.println("Number of comparisions " + minmax.getComparisons());
		
	}

}